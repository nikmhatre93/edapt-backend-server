import jwt
from app import app, logger


def createToken(payload):
    print(payload)
    token = jwt.encode(payload, app.config.get(
        'SECRET_KEY'), algorithm='HS256').decode('utf-8')
    return token


def getPayload(token):
    try:
        payload = jwt.decode(token, app.config.get(
            'SECRET_KEY'), algorithms=['HS256'])
    except jwt.ExpiredSignature as e:
        logger.error('Signature verification failed, Invalid Signature')
        payload = None
    except jwt.DecodeError as e:
        logger.error('Error decoding payload')
        payload = None

    return payload
