#!/usr/bin/env python

import urllib
import json
from app import app, logger
import requests


class Messaging():
    """Handles the Messaging for Tweeny"""

    def sendOTP(self, country_code, number, code, first_name='User'):

        
        country_code = country_code[1:]

        msg = """Hi """+str(first_name)+""", Thank you for joining Edapt!, Please use OTP """+str(code)+""" to verify your mobile number."""
        url = """
        http://api.smsala.com/api/SendSMS?api_id={}&api_password={}&sms_type=T&encoding=T&sender_id={}&phonenumber={}&textmessage={}""".format(
            app.config.get("SMSALA_APIID"),
            app.config.get("SMSALA_APIPASS"),
            app.config.get("SMSALA_SENDERID"),
            str(country_code) + str(number),
            msg
            )

        # request_body = {
        #     "api_id": app.config.get("SMSALA_APIID"),
        #     "api_password": 'Walhekar12',
        #     "phonenumber": str(country_code) + str(number),
        #     "sender_id": app.config.get("SMSALA_SENDERID"),
        #     "sms_type": "T",
        #     "encoding": "T",
        #     "templateid": None,
        #     "textmessage": "Hi {}, Thank you for joining Edapt!, Please use OTP {} to verify your mobile number.".format(first_name, code),
        #     "V1": None,
        #     "V2": None,
        #     "V3": None,
        #     "V4": None,
        #     "V5": None
        # }

        # """
        # http://api.smsala.com/api/SendSMS?api_id=API72060304001&api_password=Walhekar12&sms_type=T&encoding=T&sender_id=smsala&phonenumber=+919773973517&textmessage=test
        # """

        # api_response = requests.post('http://api.smsala.com/api/SendSMS', json=request_body)

        api_response = requests.get(url)

        # print(api_response.content,api_response.content)
        # response.content = b'{"message_id":46841,"status":"S","remarks":"Message Submitted Sucessfully"}'

        # app.logger.error("send otp response: ", api_response.content.decode)

        return json.loads(api_response.content.decode())
