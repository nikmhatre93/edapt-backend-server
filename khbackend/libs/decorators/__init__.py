from flask import Flask, request, jsonify, redirect

from functools import wraps
from functools import update_wrapper
from libs import tokenize
from datetime import timedelta

from flask import make_response, request, current_app

from app import app, db, logger


def allowed_api_roles(roles):
    def decorated(f):
        def api_authentication_required(*args, **kwargs):
            token = request.headers.get("Authorization", None)

            # app.logger.error(token)

            if token is None:
                return jsonify(
                    message="Authorization header missing could not validate the user.",
                    status=False,
                    code=401
                )

            payload = tokenize.getPayload(token)

            # app.logger.error(["token - payload : ", payload])

            if payload is None:
                return jsonify(
                    message="Invalid token request, refresh the token.",
                    status=False,
                    code=401
                )
            # check is role has the permission

            is_valid = False

            for role in payload['role']:
                if role in roles:
                    is_valid = True

            if not is_valid:
                return jsonify(
                    message="Authentication failure, not authorized.",
                    status=False,
                    code=401
                )

            kwargs['payload'] = payload
            r = f(*args, **kwargs)
            return r

        return update_wrapper(api_authentication_required, f)

    return decorated


def allowed_page_roles(roles):
    def decorated(f):
        def page_authentication_required(*args, **kwargs):

            if 'auth_token' in request.cookies:
                auth_token = request.cookies['auth_token']
                payload = tokenize.getPayload(auth_token)
                if payload is None:
                    # return jsonify(
                    #     message="Invalid token request, refresh the token.",
                    #     status=False,
                    #     code=401
                    # )
                    return redirect("/")

                # check is role has the permission

                if all(x not in roles for x in payload["role"]):
                    # return jsonify(
                    #     message="Authentication failure, not authorized.",
                    #     status=False,
                    #     code=401
                    # )
                    return redirect("/")
            else:
                # return jsonify(
                #     message="Authorization header misssing could not validate the user.",
                #     status=False,
                #     code=401
                # )
                return redirect("/")

            kwargs['payload'] = payload
            r = f(*args, **kwargs)
            return r

        return update_wrapper(page_authentication_required, f)

    return decorated


def admin_authentication_required(f):
    @wraps(f)
    def AdminDecorated(*args, **kwargs):
        token = request.headers.get("X-Admin-Key", None)
        if token is None:
            return jsonify(
                message="Admin key missing could not validate the user.",
                status=False,
                code=401
            )

        if str(token) != str(app.config.get("ADMIN_AUTH_KEY")):
            return jsonify(
                message="Invalid Admin key, refresh the token.",
                status=False,
                code=401
            )
        r = f(*args, **kwargs)
        return r

    return AdminDecorated


def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)

    return decorator
