"""
sendgrid integration
"""
import os
import sendgrid
from sendgrid.helpers.mail import *
from flask import Flask, jsonify
from app import app


class Email_:

	def __init__(self, params=None):
		# super(Email_, self).__init__()
		self.template = params

	def send_mail(self, params=None):

		try:
			sg = sendgrid.SendGridAPIClient(apikey=app.config.get("SENDGRID_APIKEY"))
			from_email = Email("admin@khedapt.com")
			to_email = Email(params["to_email"])
			subject = self.template["subject"]
			content = Content(
			    "text/html", self.template["message"])
			mail = Mail(from_email, subject, to_email, content)
			response = sg.client.mail.send.post(request_body=mail.get())

			json_response = {
				"status": True,
				"code": response.status_code,
				"message": "email sent to "+params["to_email"]
			}

		except Exception as e:
			app.logger.error("exception here", e)

			json_response = {
				"status": False,
				"code": response.status_code,
				"message": "email sent to "+params["to_email"]
			}

		return json_response