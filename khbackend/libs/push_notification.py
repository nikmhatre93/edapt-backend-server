from pyfcm import FCMNotification
from app import app

push_service = FCMNotification(api_key=app.config.get("FCM_SERVER_KEY"))


class PushNotification:

    def __init__(self, registration_id):
        self.registration_id = registration_id

    def send_display_message(self, params):
        message_title = params["title"]
        message_body = params["message"]
        result = push_service.notify_single_device(
            registration_id=self.registration_id, message_title=message_title, message_body=message_body, sound='default')
        return result

    def send_data_message(self, params):
        data_message = {
            "title": params["title"]
        }
        message_body = params["message"]
        result = push_service.notify_single_device(
            registration_id=self.registration_id, message_body=message_body, data_message=data_message, sound='default')
        return result
