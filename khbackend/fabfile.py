from fabric.api import *

env.user = 'ubuntu'
env.hosts = [
    'ec2-18-220-174-26.us-east-2.compute.amazonaws.com'
]

def deploy(app='staging'):
	if app == 'staging':
		APP_DIR = '/var/www/html/services/khbackend'
		APP_RESTART = 'sudo initctl restart knowhassels'
	
	elif app == 'master':
		APP_DIR = '/home/ubuntu/khbackend'
		APP_RESTART = 'sudo initctl restart knowhassels-prod'
	
	GIT_PULL = ' '.join(['sudo git pull origin', app])
	
	with cd(APP_DIR):
		run(GIT_PULL)
		run(APP_RESTART)