# APP_BASE_URL = "http://18.220.174.26:5000"
# APP_BASE_URL = "http://18.219.104.226:5000"
APP_BASE_URL = "http://18.223.13.227"

APP_ROLES = {
    "ADMIN": "kh_admin",
    "STUDENT": "kh_student",
    "BUDDY": "kh_buddy",
    "EVENTMGR": "kh_event_mgr"
}

USER_INFO_FIELDS = [
    "_id",
    "username",
    "image",
    "first_name",
    "last_name",
    "sex",
    "about",
    "country_code",
    "contact",
    "email",
    "home_address",
    "home_town",
    "current_city",
    "city",
    "origin",
    "university",
    "country",
    "date_of_birth",
    "mother_tongue",
    "languages",
    "education",
    "employment",
    "is_contact_verified",
    "roles",
    "created",
    "updated",
    "is_active",
    'lastSeen',
    'deviceType',
    'notification',
    'interests',
    'age',
    'university'
]

STUDENT_INFO_FOR_BUDDY = [
    "user_id",
    "arrival_city",
    "arrival_country",
    # "arrival_dt",
    "arrival_terminal",
    # "departure_dt",
    "departure_terminal",
    "air_ticket",
    "passport_img",
    "visa_img",
    "services",
    "travelling_year",
    "travelling_month",
    "visible"
]

STUDENT_INFO_FOR_STUDENT = [
    "user_id",
    "arrival_city",
    "arrival_country",
    "arrival_dt",
    "departure_dt",
    "arrival_province",
    "travelling_year",
    "travelling_month",
    "university",
    "visible",
    "arrival_terminal",
    "departure_terminal",
    "student_availability",
    "buddy_availability",
    "hired_buddy",
    # "created"
]

BUDDY_INFO_FOR_STUDENT = [
    "user_id",
    # "service_start",
    # "service_end",
    "service_at_city",
    "service_at_country",
    "service_at_province",
    "service_month",
    "service_year",
    "package_id",
    "is_package_selected",
    "is_verified"
    # "created"
]

BUDDY_INFO_FOR_BUDDY = [
    "user_id",
    # "service_start",
    # "service_end",
    "service_at_city",
    "service_at_country",
    "service_at_province",
    "service_month",
    "service_year",
    "package_id",
    "is_verified",
    "is_package_selected",
    "is_bank_added"
]

USER_UPDATE_FIELDS = [
    "first_name",
    "last_name",
    "contact",
    "country_code",
    "email",
    "image",
    "about",
    "sex",
    "date_of_birth",
    "city",
    "country",
    "is_active",
    "home_address",
    "origin",
    "mother_tongue",
    "university",
    "date_of_birth",
    "mother_toungue",
    "languages",
    "employment",
    "education",
    "interests",
    "age",
    "university"
]

# SERVICES = [
#     "Airport Receiving",
#     "Accomodation Verification",
#     "Accomodation Booking Process",
#     "Accmodation hunting assistance",
#     "Counselling & Guidance",
#     "GIC - Bank Account Setup",
#     "SIN",
#     "Provincial IDs",
#     "Metro-Pass & Student ID",
#     "Grocery ",
#     "Bedding & Accessories",
#     "City Tour"
# ]

SERVICES = [
    'Accomodation assistance',
    'Counselling & Guidance',
    # 'Handholding in',
    'Bank Account Setup',
    'Social Insurance Number',
    'Provincial IDs',
    'Transportation Passes',
    'SIM & Internet',
    'Initial Grocery',
    'Bedding & Accessories'
]

ALLOWED_EXTENSIONS = set(['pdf', 'png', 'jpg', 'jpeg'])

REQUIRED_USER_ATTRIBUTES = [
    'first_name',
    'last_name',
    'sex',
    'city',
    'origin',
    # 'home_town',
    'age',
    'contact',
    'email',
    'home_address',
    # 'current_city',
    'education',
    'about',
    'university',
    'languages'
    # 'current_country'
]

REQUIRED_TRAVELLING_ATTRIBUTES = [
    # 'travelling_month',
    # 'travelling_year',
    # 'arrival_city',
    # 'arrival_dt',
    # 'departure_dt',
    # 'arrival_terminal',
    # 'departure_terminal',
    # 'arrival_province',
    # 'arrival_country'
]

REQUIRED_SERVICING_ATTRIBUTES = [
    # 'package_id',
    # 'service_at_city',
    # 'service_at_country',
    # 'service_at_province',
    # 'service_year',
    # 'service_month',
]

PROVINCES = {
    "Canada": ["Ontario", "British Columbia", "Alberta"]
}

CITIES = {
    "Ontario": ["Barrie", "Bolleville", "Hamilton", "Kingston", "Kitchener", "London", "North Bay",
                "Oakville", "Oshawa", "Ottawa", "Peterborough", "Sarnia", "Sault Ste. Marie",
                "South Porcupine", "Sudbury", "Thunderbay", "Toronto", "Welland", "Windsor"],
    "British Columbia": ["Abbotsford", "Castlegar", "Cranbrook", "Dawson Creek", "Nanaimo",
                         "New Westminster", "Prince George", "Salmon Arm", "Surrey", "Vancouver",
                         "Vancouver Island", "Victoria"],
    "Alberta": ["Calagary", "Vermilion", "Edmonto"]
}

YEARS = ["2017", "2018", "2019", "2020"]
MONTHS = {"Jan": 1, "May": 5, "Sep": 9}
# MONTHS = [1, 5, 9]

UNIVERSITY = ""

COUNTRY_CODES = {
    'Afghanistan': '93',
    'Albania': '355',
    'Algeria': '213',
    'American Samoa': '1-684',
    'Andorra': '376',
    'Angola': '244',
    'Anguilla': '1-264',
    'Antarctica': '672',
    'Antigua and Barbuda': '1-268',
    'Argentina': '54',
    'Armenia': '374',
    'Aruba': '297',
    'Australia': '61',
    'Austria': '43',
    'Azerbaijan': '994',
    'Bahamas': '1-242',
    'Bahrain': '973',
    'Bangladesh': '880',
    'Barbados': '1-246',
    'Belarus': '375',
    'Belgium': '32',
    'Belize': '501',
    'Benin': '229',
    'Bermuda': '1-441',
    'Bhutan': '975',
    'Bolivia': '591',
    'Bonaire': '599',
    'Bosnia and Herzegovina': '387',
    'Botswana': '267',
    'Bouvet Island': '47',
    'Brazil': '55',
    'British Indian Ocean Territory': '246',
    'British Virgin Islands': '1-284',
    'Brunei Darussalam': '673',
    'Bulgaria': '359',
    'Burkina Faso': '226',
    'Burundi': '257',
    'Cambodia': '855',
    'Cameroon': '237',
    'Canada': '1',
    'Cape Verde': '238',
    'Cayman Islands': '1-345',
    'Central African Republic': '236',
    'Chad': '235',
    'Chile': '56',
    'China': '86',
    'Christmas Island': '61',
    'Cocos (Keeling) Islands': '61',
    'Colombia': '57',
    'Comoros': '269',
    'Congo': '242',
    'Cook Islands': '682',
    'Costa Rica': '506',
    "Cote d'Ivoire": '225',
    'Croatia': '385',
    'Cuba': '53',
    'Curacao': '599',
    'Cyprus': '357',
    'Czech Republic': '420',
    'Democratic Republic of the Congo': '243',
    'Denmark': '45',
    'Djibouti': '253',
    'Dominica': '1-767',
    'Dominican Republic': '1-809,1-829,1-849',
    'Ecuador': '593',
    'Egypt': '20',
    'El Salvador': '503',
    'Equatorial Guinea': '240',
    'Eritrea': '291',
    'Estonia': '372',
    'Ethiopia': '251',
    'Falkland Islands (Malvinas)': '500',
    'Faroe Islands': '298',
    'Fiji': '679',
    'Finland': '358',
    'France': '33',
    'French Guiana': '594',
    'French Polynesia': '689',
    'French Southern Territories': '262',
    'Gabon': '241',
    'Gambia': '220',
    'Georgia': '995',
    'Germany': '49',
    'Ghana': '233',
    'Gibraltar': '350',
    'Greece': '30',
    'Greenland': '299',
    'Grenada': '1-473',
    'Guadeloupe': '590',
    'Guam': '1-671',
    'Guatemala': '502',
    'Guernsey': '44',
    'Guinea': '224',
    'Guinea-Bissau': '245',
    'Guyana': '592',
    'Haiti': '509',
    'Heard Island and McDonald Mcdonald Islands': '672',
    'Holy See (Vatican City State)': '379',
    'Honduras': '504',
    'Hong Kong': '852',
    'Hungary': '36',
    'Iceland': '354',
    'India': '91',
    'Indonesia': '62',
    'Iran, Islamic Republic of': '98',
    'Iraq': '964',
    'Ireland': '353',
    'Isle of Man': '44',
    'Israel': '972',
    'Italy': '39',
    'Jamaica': '1-876',
    'Japan': '81',
    'Jersey': '44',
    'Jordan': '962',
    'Kazakhstan': '7',
    'Kenya': '254',
    'Kiribati': '686',
    "Korea, Democratic People's Republic of": '850',
    'Korea, Republic of': '82',
    'Kuwait': '965',
    'Kyrgyzstan': '996',
    "Lao People's Democratic Republic": '856',
    'Latvia': '371',
    'Lebanon': '961',
    'Lesotho': '266',
    'Liberia': '231',
    'Libya': '218',
    'Liechtenstein': '423',
    'Lithuania': '370',
    'Luxembourg': '352',
    'Macao': '853',
    'Macedonia, the Former Yugoslav Republic of': '389',
    'Madagascar': '261',
    'Malawi': '265',
    'Malaysia': '60',
    'Maldives': '960',
    'Mali': '223',
    'Malta': '356',
    'Marshall Islands': '692',
    'Martinique': '596',
    'Mauritania': '222',
    'Mauritius': '230',
    'Mayotte': '262',
    'Mexico': '52',
    'Micronesia, Federated States of': '691',
    'Moldova, Republic of': '373',
    'Monaco': '377',
    'Mongolia': '976',
    'Montenegro': '382',
    'Montserrat': '1-664',
    'Morocco': '212',
    'Mozambique': '258',
    'Myanmar': '95',
    'Namibia': '264',
    'Nauru': '674',
    'Nepal': '977',
    'Netherlands': '31',
    'New Caledonia': '687',
    'New Zealand': '64',
    'Nicaragua': '505',
    'Niger': '227',
    'Nigeria': '234',
    'Niue': '683',
    'Norfolk Island': '672',
    'Northern Mariana Islands': '1-670',
    'Norway': '47',
    'Oman': '968',
    'Pakistan': '92',
    'Palau': '680',
    'Palestine, State of': '970',
    'Panama': '507',
    'Papua New Guinea': '675',
    'Paraguay': '595',
    'Peru': '51',
    'Philippines': '63',
    'Pitcairn': '870',
    'Poland': '48',
    'Portugal': '351',
    'Puerto Rico': '1',
    'Qatar': '974',
    'Reunion': '262',
    'Romania': '40',
    'Russian Federation': '7',
    'Rwanda': '250',
    'Saint Barthelemy': '590',
    'Saint Helena': '290',
    'Saint Kitts and Nevis': '1-869',
    'Saint Lucia': '1-758',
    'Saint Martin (French part)': '590',
    'Saint Pierre and Miquelon': '508',
    'Saint Vincent and the Grenadines': '1-784',
    'Samoa': '685',
    'San Marino': '378',
    'Sao Tome and Principe': '239',
    'Saudi Arabia': '966',
    'Senegal': '221',
    'Serbia': '381',
    'Seychelles': '248',
    'Sierra Leone': '232',
    'Singapore': '65',
    'Sint Maarten (Dutch part)': '1-721',
    'Slovakia': '421',
    'Slovenia': '386',
    'Solomon Islands': '677',
    'Somalia': '252',
    'South Africa': '27',
    'South Georgia and the South Sandwich Islands': '500',
    'South Sudan': '211',
    'Spain': '34',
    'Sri Lanka': '94',
    'Sudan': '249',
    'Suriname': '597',
    'Svalbard and Jan Mayen': '47',
    'Swaziland': '268',
    'Sweden': '46',
    'Switzerland': '41',
    'Syrian Arab Republic': '963',
    'Taiwan, Province of China': '886',
    'Tajikistan': '992',
    'Thailand': '66',
    'Timor-Leste': '670',
    'Togo': '228',
    'Tokelau': '690',
    'Tonga': '676',
    'Trinidad and Tobago': '1-868',
    'Tunisia': '216',
    'Turkey': '90',
    'Turkmenistan': '993',
    'Turks and Caicos Islands': '1-649',
    'Tuvalu': '688',
    'US Virgin Islands': '1-340',
    'Uganda': '256',
    'Ukraine': '380',
    'United Arab Emirates': '971',
    'United Kingdom': '44',
    'United Republic of Tanzania': '255',
    'United States': '1',
    'United States Minor Outlying Islands': '1',
    'Uruguay': '598',
    'Uzbekistan': '998',
    'Vanuatu': '678',
    'Venezuela': '58',
    'Viet Nam': '84',
    'Wallis and Futuna': '681',
    'Western Sahara': '212',
    'Yemen': '967',
    'Zambia': '260',
    'Zimbabwe': '263'
}

LANGUAGES = [
  "Abkhaz",
  "Afar",
  "Afrikaans",
  "Akan",
  "Albanian",
  "Amharic",
  "Arabic",
  "Aragonese",
  "Armenian",
  "Assamese",
  "Avaric",
  "Avestan",
  "Aymara",
  "Azerbaijani",
  "Bambara",
  "Bashkir",
  "Basque",
  "Belarusian",
  "Bengali",
  "Bihari",
  "Bislama",
  "Bosnian",
  "Breton",
  "Bulgarian",
  "Burmese",
  "Catalan; Valencian",
  "Chamorro",
  "Chechen",
  "Chichewa; Chewa; Nyanja",
  "Chinese",
  "Chuvash",
  "Cornish",
  "Corsican",
  "Cree",
  "Croatian",
  "Czech",
  "Danish",
  "Divehi; Dhivehi; Maldivian;",
  "Dutch",
  "English",
  "Esperanto",
  "Estonian",
  "Ewe",
  "Faroese",
  "Fijian",
  "Finnish",
  "French",
  "Fula; Fulah; Pulaar; Pular",
  "Galician",
  "Georgian",
  "German",
  "Greek, Modern",
  "Guaraní",
  "Gujarati",
  "Haitian; Haitian Creole",
  "Hausa",
  "Hebrew (modern)",
  "Herero",
  "Hindi",
  "Hiri Motu",
  "Hungarian",
  "Interlingua",
  "Indonesian",
  "Interlingue",
  "Irish",
  "Igbo",
  "Inupiaq",
  "Ido",
  "Icelandic",
  "Italian",
  "Inuktitut",
  "Japanese",
  "Javanese",
  "Kalaallisut, Greenlandic",
  "Kannada",
  "Kanuri",
  "Kashmiri",
  "Kazakh",
  "Khmer",
  "Kikuyu, Gikuyu",
  "Kinyarwanda",
  "Kirghiz, Kyrgyz",
  "Komi",
  "Kongo",
  "Korean",
  "Kurdish",
  "Kwanyama, Kuanyama",
  "Latin",
  "Luxembourgish, Letzeburgesch",
  "Luganda",
  "Limburgish, Limburgan, Limburger",
  "Lingala",
  "Lao",
  "Lithuanian",
  "Luba-Katanga",
  "Latvian",
  "Manx",
  "Macedonian",
  "Malagasy",
  "Malay",
  "Malayalam",
  "Maltese",
  "Māori",
  "Marathi (Marāṭhī)",
  "Marshallese",
  "Mongolian",
  "Nauru",
  "Navajo, Navaho",
  "Norwegian Bokmål",
  "North Ndebele",
  "Nepali",
  "Ndonga",
  "Norwegian Nynorsk",
  "Norwegian",
  "Nuosu",
  "South Ndebele",
  "Occitan",
  "Ojibwe, Ojibwa",
  "Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic",
  "Oromo",
  "Oriya",
  "Ossetian, Ossetic",
  "Panjabi, Punjabi",
  "Pāli",
  "Persian",
  "Polish",
  "Pashto, Pushto",
  "Portuguese",
  "Quechua",
  "Romansh",
  "Kirundi",
  "Romanian, Moldavian, Moldovan",
  "Russian",
  "Sanskrit (Saṁskṛta)",
  "Sardinian",
  "Sindhi",
  "Northern Sami",
  "Samoan",
  "Sango",
  "Serbian",
  "Scottish Gaelic; Gaelic",
  "Shona",
  "Sinhala, Sinhalese",
  "Slovak",
  "Slovene",
  "Somali",
  "Southern Sotho",
  "Spanish; Castilian",
  "Sundanese",
  "Swahili",
  "Swati",
  "Swedish",
  "Tamil",
  "Telugu",
  "Tajik",
  "Thai",
  "Tigrinya",
  "Tibetan Standard, Tibetan, Central",
  "Turkmen",
  "Tagalog",
  "Tswana",
  "Tonga (Tonga Islands)",
  "Turkish",
  "Tsonga",
  "Tatar",
  "Twi",
  "Tahitian",
  "Uighur, Uyghur",
  "Ukrainian",
  "Urdu",
  "Uzbek",
  "Venda",
  "Vietnamese",
  "Volapük",
  "Walloon",
  "Welsh",
  "Wolof",
  "Western Frisian",
  "Xhosa",
  "Yiddish",
  "Yoruba",
  "Zhuang, Chuang"
]

EMAIL_TEMPLATES = {
    "signup_student": {
        "subject": "Welcome to Edapt!",
        "message": """
<p style="padding:20px;color:#333333">
Congratulations, {FIRST_NAME}!<br><br>
You have just joined Edapt - a global community of International students traveling to Canada.<br><br>
Edapt is one stop solution for the international students who want to settle in Canada.<br><br>
For this, Edapt uses its own uniquely designed social networking platform to help the new students connect with local mentors &amp; fellow aspirants.<br><br>
Intro video: <a href="https://youtu.be/TcF0xQ0uX8k">https://youtu.be/TcF0xQ0uX8k</a><br><br>
We are very pleased to inform you that your account is all set, just verify your email address by clicking the link below and start exploring.<br><br>
<a href="{URL}">{URL}</a><br><br>
We wish you luck in your new endeavor.<br><br>
See you on Edapt!<br><br>
<a href="https://www.facebook.com/KHEdapt">Facebook</a>&nbsp;
<a href="https://www.instagram.com/edapt_app">Instagram</a>&nbsp;
<a href="https://www.linkedin.com/company/edaptapp">Linkedin</a>&nbsp;
<a href="https://twitter.com/AppEdapt">Twitter</a>&nbsp;<br><br>
<span style="font-size: 11px;color:#555555">Copyright © 2018 Edapt, All rights reserved.<br>
You received this email because you registered on Edapt.<br>
Our mailing address is, KnowHassles Edapt Private Limited, 304 Panama Planet, Gokhale Road, Thane West, Maharashtra, India. 400602.</span>
</p>
"""
    },
    "signup_buddy": {
        "subject": "Welcome to Edapt!",
        "message": """
<p style="padding:20px;color:#333333">
Congratulations, {FIRST_NAME}!<br><br>
You have just joined Edapt - a global community of International students and local mentors in Canada.<br><br>
Edapt connects local mentors with the new International students who are looking for one on one assistance in their transition from one country to another.<br><br>
As a mentor, you are responsible to provide support in on arrival challenges faced by them.<br><br>
Intro video: <a href="https://youtu.be/KRs5NN0RW8A">https://youtu.be/KRs5NN0RW8A</a><br><br>
We are very pleased to inform you that your account is almost ready, just verify your email address by clicking the link below and follow the instructions in the App to get going.<br><br>
<a href="{URL}">{URL}</a><br><br>
We wish you luck in your new endeavor.<br><br>
See you on Edapt!<br><br>
<a href="https://www.facebook.com/KHEdapt">Facebook</a>&nbsp;
<a href="https://www.instagram.com/edapt_app">Instagram</a>&nbsp;
<a href="https://www.linkedin.com/company/edaptapp">Linkedin</a>&nbsp;
<a href="https://twitter.com/AppEdapt">Twitter</a>&nbsp;<br><br>
<span style="font-size: 11px;color:#555555">Copyright © 2018 Edapt, All rights reserved.<br>
You received this email because you registered on Edapt.<br>
Our mailing address is, KnowHassles Edapt Private Limited, 304 Panama Planet, Gokhale Road, Thane West, Maharashtra, India. 400602.</span>
</p>
"""
    },
    "hired_buddy": {
        "subject": "Congratulations! You have a new client...",
        "message": """
<p style="padding:20px;color:#333333">
Hi, {MENTOR_NAME}!<br><br>
Congratulations! You have been picked by {STUDENT_NAME} as a mentor for {PACKAGE_NAME}<br><br>
You can now see {STUDENT_NAME} in the ‘Your clients’ section.<br><br>
We wish you luck in your new endeavor.<br><br>
Start interacting with them and offer unmatched service quality and support to get more and more business from Edapt.<br><br>
Please do not forget to check the box for every given service in the service panel and remind your client to do the same.<br><br>
You are requested to complete all the services within 15 days upon the client’s arrival in {ARRIVAL_CITY}.<br><br>
Happy mentoring!<br><br>
-<br><br>
Team Edapt<br><br>
<a href="https://www.facebook.com/KHEdapt">Facebook</a>&nbsp;
<a href="https://www.instagram.com/edapt_app">Instagram</a>&nbsp;
<a href="https://www.linkedin.com/company/edaptapp">Linkedin</a>&nbsp;
<a href="https://twitter.com/AppEdapt">Twitter</a>&nbsp;<br><br>
<span style="font-size: 11px;color:#555555">Copyright © 2018 Edapt, All rights reserved.<br>
You received this email because you registered on Edapt.<br>
Our mailing address is, KnowHassles Edapt Private Limited, 304 Panama Planet, Gokhale Road, Thane West, Maharashtra, India. 400602.</span>
</p>
"""
    },
    "account_activated_buddy": {
        "subject": "Congratulations! Your mentor profile is now active…",
        "message": """
<p style="padding:20px;color:#333333">
Hi, {MENTOR_NAME}!<br><br>
Congratulations!<br><br>
Your mentor profile has been approved after verifying your documents.<br><br>
Please read ‘How it works’ and ‘FAQs’ sections carefully before serving your clients.<br><br>
Happy mentoring!<br><br>
-<br><br>
Team Edapt<br><br>
<a href="https://www.facebook.com/KHEdapt">Facebook</a>&nbsp;
<a href="https://www.instagram.com/edapt_app">Instagram</a>&nbsp;
<a href="https://www.linkedin.com/company/edaptapp">Linkedin</a>&nbsp;
<a href="https://twitter.com/AppEdapt">Twitter</a>&nbsp;<br><br>
<span style="font-size: 11px;color:#555555">Copyright © 2018 Edapt, All rights reserved.<br>
You received this email because you registered on Edapt.<br>
Our mailing address is, KnowHassles Edapt Private Limited, 304 Panama Planet, Gokhale Road, Thane West, Maharashtra, India. 400602.</span>
</p>
"""
    },
    "account_deactivated_buddy": {
        "subject": "Your Edapt account is deactivated…",
        "message": """
<p style="padding:20px;color:#333333">
Hi, {MENTOR_NAME}!<br><br>
Unfortunately , your Edapt account has been deactivated.<br><br>
Please contact the admin at <a href="mailto:support@khedapt.com">support@khedapt.com</a> for further details.<br><br>
-<br><br>
Team Edapt<br><br>
<a href="https://www.facebook.com/KHEdapt">Facebook</a>&nbsp;
<a href="https://www.instagram.com/edapt_app">Instagram</a>&nbsp;
<a href="https://www.linkedin.com/company/edaptapp">Linkedin</a>&nbsp;
<a href="https://twitter.com/AppEdapt">Twitter</a>&nbsp;<br><br>
<span style="font-size: 11px;color:#555555">Copyright © 2018 Edapt, All rights reserved.<br>
You received this email because you registered on Edapt.<br>
Our mailing address is, KnowHassles Edapt Private Limited, 304 Panama Planet, Gokhale Road, Thane West, Maharashtra, India. 400602.</span>
</p>
"""
    },
    "buddy_reported": {
        "subject": "Alert - Client has addressed an issue",
        "message": """
<p style="padding:20px;color:#333333">
Hi, {MENTOR_NAME}!<br><br>
We are not very pleased to inform you that your current client {STUDENT_NAME} has reported an issue about your service.<br><br>
We are reviewing the situation at our end and will soon get in touch with you.<br><br>
Your service is temporarily suspended until the further notice.<br><br>
Thanks a lot for your patience.<br><br>
-<br><br>
Team Edapt<br><br>
<a href="https://www.facebook.com/KHEdapt">Facebook</a>&nbsp;
<a href="https://www.instagram.com/edapt_app">Instagram</a>&nbsp;
<a href="https://www.linkedin.com/company/edaptapp">Linkedin</a>&nbsp;
<a href="https://twitter.com/AppEdapt">Twitter</a>&nbsp;<br><br>
<span style="font-size: 11px;color:#555555">Copyright © 2018 Edapt, All rights reserved.<br>
You received this email because you registered on Edapt.<br>
Our mailing address is, KnowHassles Edapt Private Limited, 304 Panama Planet, Gokhale Road, Thane West, Maharashtra, India. 400602.</span>
</p>
"""
    },
    "forgotpassword": {
        "subject": "Recover your Edapt account…",
        "message": """
<p style="padding:20px;color:#333333">
Hey {FIRST_NAME}!<br><br>
Ah! Remembering password can never be an easy task. Don't you worry!<br>
This is your password reset token:<br><br>
<b>{TOKEN}</b><br><br>
Copy this token and paste it in your Edapt app to change your password.<br><br>
See you on Edapt.<br><br>
-<br><br>
Team Edapt<br><br>
<a href="https://www.facebook.com/KHEdapt">Facebook</a>&nbsp;
<a href="https://www.instagram.com/edapt_app">Instagram</a>&nbsp;
<a href="https://www.linkedin.com/company/edaptapp">Linkedin</a>&nbsp;
<a href="https://twitter.com/AppEdapt">Twitter</a>&nbsp;<br><br>
<span style="font-size: 11px;color:#555555">Copyright © 2018 Edapt, All rights reserved.<br>
You received this email because you registered on Edapt.<br>
Our mailing address is, KnowHassles Edapt Private Limited, 304 Panama Planet, Gokhale Road, Thane West, Maharashtra, India. 400602.</span>
</p>
"""
    },
    "report_issue_admin": {
        "subject" : "Mentor Reported",
        "message" : "Hey Admin, {} reported issue against {}, which says {}"
    },
    "feedback":{

        "subject" : "Feedback from a User",
        "message" : """Feedback from, {name}, 
                        which says {msg}"""
    },
    "report_user":{

        "subject" : "User Reported",
        "message" : """Username: {username}, 
                        name {name}"""
    }

}

TESTIMONIALS = [{
    "name": "Sahil Jhaveri",
    "description": """I still remember the day when I landed in Canada on August 24, 2016. I was surely confident that I would not get lost as soon as I'll step out of the airport. Trust me, I wasn't nervous at all because Edapt founder, Harshad Walhekar himself came to receive me at the airport. Edapt is always committed for providing valuable services to their customers in terms of getting the best deal including but not limited to renting a house or helping students with legal services, and of course entertainment. I am highly delighted by looking at the services what Edapt have provided me and I am sure they'll never let my hand go even if I face any problem in the future. 
I truly thank Edapt team & the mentors for holding my hand and showing me right direction to settle in International country like Canada and I truly wish them all the very best for keeping up the good work as always
Thank you,"""
}, {
    "name": "Vibhor Trivedi",
    "description": """Leaving your loved ones and your country is always the toughest decision to make but when it comes to your future the only option you are left with is to opt the toughest choice. Then comes the role of “Edapt” where they leave no stone unturned to make the students life secure by giving them a safe place to stay, helps them to obtain their identity in a new strange environment. They helped me from picking up me from the airport till the time I was independent and settled down here as an individual. The best part about them is, even they know how it feels when someone comes 11,600 kilometres away from their comfort zone and how to establish yourself that is why they are called “Know Hassles”, they know your hassles and they are ready with every solution."""
}, {
    "name": "Bhavana Irava",
    "description": """Edapt is very useful for students that are new to the country. It helps you settle down initially by providing basic needs and guidelines to figure out everything else and that is not only on emails or calls.They provide actual hand-holding.
Being a girl student, I was very nervous initially so were my parents but The team has been very friendly and accommodating. It was a pleasant experience with the company."""
}, {
    "name": "Joseph Godson",
    "description": """All new international students must know Edapt. They are awesome!!
This is very useful for me as I had no one to support here in Toronto but their mentors did everything for me."""
}, {
    "name": "Neil Grover",
    "description": """The dedication of its employees towards its students is something all the other student handling related businesses should learn. Made my day!! Just keep up the good work, don't let the spirit die."""
}, {
    "name": "Dheeraj Dandekar",
    "description": """I would like to thank the entire team of Edapt for a smooth and comfortable transition into Canada. From housing to finding my first job in Canada, Edapt have indeed provided with an exceptional and satisfactory service. I would definitely recommend Edapt for international students seeking a helping hand in foreign lands. Keep up the good work guys.
I am looking  to help new students by being mentor at Edapt as it is really a noble cause. Really a wonderful platform to build the network and get accustomed to people and make friends too."""
}, {
    "name": "Arun George",
    "description": """It is simple ..perfect!!"""
}, {
    "name": "Saurabh Wargaonkar",
    "description": """Care and concern is extraordinary ! Keep up the good work guys!!"""
}, {
    "name": "Ann Mary",
    "description": """The best part is letting us know in advance how we should prepare for the culture change here!"""
}, {
    "name": "Athal Nair",
    "description": """Very friendly supportive and was always there whenever i needed them!!"""
}]

INTERESTS = ["Clean","Healthy","Non-smoker","Professional","Student","Early bird","Night owl","Social butterfly","Gym rat","Creative","Foodie","Beach bum","Bicyclist","Party animal","Tree hugger"]

UNIVERSITIES = [
 {  
    'name': "British Columbia",
    'children': [{
        'name': "Ashton College",
        'id': "Ashton College"
      },{
        'name': "Brighton College",
        'id': "Brighton College"
      },{
        'name': "British Columbia Inst. of Technology",
        'id': "British Columbia Inst. of Technology"
      },{
        'name': "British Columbia Open University",
        'id': "British Columbia Open University"
      },{
        'name': "Camosun College",
        'id': "Camosun College"
      },{
        'name': "Canadian College",
        'id': "Canadian College"
      },{
        'name': "Capilano University",
        'id': "Capilano University"
      },{
        'name': "Collège Éducacentre",
        'id': "Collège Éducacentre"
      },{
        'name': "College of New Caledonia",
        'id': "College of New Caledonia"
      },{
        'name': "College of the Rockies",
        'id': "College of the Rockies"
      },{
        'name': "Columbia International College",
        'id': "Columbia International College"
      },{
        'name': "Douglas College",
        'id': "Douglas College"
      },{
        'name': "Eton College",
        'id': "Eton College"
      },{
        'name': "Justice Institute of B.C.",
        'id': "Justice Institute of B.C."
      },{
        'name': "Kwantlen Polytechnic Uni.",
        'id': "Kwantlen Polytechnic Uni."
      },{
        'name': "Langara College",
        'id': "Langara College"
      },{
        'name': "Native Education College",
        'id': "Native Education College"
      },{
        'name': "Nicola Valley Institute",
        'id': "Nicola Valley Institute",
      },{
        'name': "North Island College",
        'id': "North Island College",
      },{
        'name': "Northern Lights College",
        'id': "Northern Lights College",
      },{
        'name': "Northwest Community College",
        'id': "Northwest Community College",
      },{
        'name': "Okanagan University College",
        'id': "Okanagan University College",
      },{
        'name': "Royal Roads University",
        'id': "Royal Roads University",
      },{
        'name': "Selkirk College",
        'id': "Selkirk College",
      },{
        'name': "Simon Fraser University",
        'id': "Simon Fraser University",
      },{
        'name': "Sprott Shaw College",
        'id': "Sprott Shaw College",
      },{
        'name': "Thompson River University",
        'id': "Thompson River University",
      },{
        'name': "Trinity Western University",
        'id': "Trinity Western University",
      },{
        'name': "University College of Cariboo",
        'id': "University College of Cariboo",
      },{
        'name': "University of British Columbia",
        'id': "University of British Columbia",
      },{
        'name': "University of Northern B.C.",
        'id': "University of Northern B.C.",
      },{
        'name': "University of Fraser Valley",
        'id': "University of Fraser Valley",
      },{
        'name': "University of Victoria",
        'id': "University of Victoria",
      },{
        'name': "Vancouver Community College",
        'id': "Vancouver Community College",
      },{
        'name': "Vancouver Institute of Media Arts",
        'id': "Vancouver Institute of Media Arts",
      },{
        'name': "Vancouver Island University",
        'id': "Vancouver Island University",
      },{
        'name': "Other",
        'id': "Other"
      }
      ]
  },
  {
    'name': "Ontario",
    'children': [{
        'name': "Algoma University",
        'id': "Algoma University",
      },{
        'name': "Algonquin College",
        'id': "Algonquin College",
      },{
        'name': "Brescia University College",
        'id': "Brescia University College",
      },{
        'name': "Brock University",
        'id': "Brock University",
      },{
        'name': "Cambrian College",
        'id': "Cambrian College",
      },{
        'name': "Canadore College",
        'id': "Canadore College",
      },{
        'name': "Carleton University",
        'id': "Carleton University",
      },{
        'name': "Centennial College",
        'id': "Centennial College",
      },{
        'name': "Clair College",
        'id': "Clair College",
      },{
        'name': "Collège Boréal",
        'id': "Collège Boréal",
      },{
        'name': "Conestoga College",
        'id': "Conestoga College",
      },{
        'name': "Confederation College",
        'id': "Confederation College",
      },{
        'name': "Dominican University College",
        'id': "Dominican University College",
      },{
        'name': "Durham College",
        'id': "Durham College",
      },{
        'name': "Fanshawe College",
        'id': "Fanshawe College",
      },{
        'name': "Fleming College",
        'id': "Fleming College",
      },{
        'name': "George Brown College",
        'id': "George Brown College",
      },{
        'name': "Georgian College",
        'id': "Georgian College",
      },{
        'name': "Humber College",
        'id': "Humber College",
      },{
        'name': "Huron University College",
        'id': "Huron University College",
      },{
        'name': "King’s University College",
        'id': "King’s University College",
      },{
        'name': "La Cité collégiale",
        'id': "La Cité collégiale",
      },{
        'name': "Lakehead University",
        'id': "Lakehead University",
      },{
        'name': "Lambton College",
        'id': "Lambton College",
      },{
        'name': "Laurentian University",
        'id': "Laurentian University",
      },{
        'name': "Lawrence College- Kingston",
        'id': "Lawrence College- Kingston",
      },{
        'name': "Loyalist College",
        'id': "Loyalist College",
      },{
        'name': "McMaster University",
        'id': "McMaster University",
      },{
        'name': "Mohawk College",
        'id': "Mohawk College",
      },{
        'name': "Niagara College",
        'id': "Niagara College",
      },{
        'name': "Nipissing University",
        'id': "Nipissing University",
      },{
        'name': "Northern College",
        'id': "Northern College",
      },{
        'name': "OCAD University",
        'id': "OCAD University",
      },{
        'name': "Queen’s University",
        'id': "Queen’s University",
      },{
        'name': "Redeemer University",
        'id': "Redeemer University",
      },{
        'name': "Royal Military College",
        'id': "Royal Military College",
      },{
        'name': "Ryerson University",
        'id': "Ryerson University",
      },{
        'name': "Saint Paul University",
        'id': "Saint Paul University",
      },{
        'name': "Sault College",
        'id': "Sault College",
      },{
        'name': "Seneca College",
        'id': "Seneca College",
      },{
        'name': "Sheridan College",
        'id': "Sheridan College",
      },{
        'name': "St. Clair College",
        'id': "St. Clair College",
      },{
        'name': "St. Jerome’s University",
        'id': "St. Jerome’s University",
      },{
        'name': "St. Lawrence College",
        'id': "St. Lawrence College",
      },{
        'name': "The Michener Institute",
        'id': "The Michener Institute",
      },{
        'name': "Trent University",
        'id': "Trent University",
      },{
        'name': "Université de Guelph- d’Alfred",
        'id': "Université de Guelph- d’Alfred",
      },{
        'name': "University of Guelph",
        'id': "University of Guelph",
      },{
        'name': "University of Ontario",
        'id': "University of Ontario",
      },{
        'name': "University of Ottawa",
        'id': "University of Ottawa",
      },{
        'name': "University of St. Michael’s",
        'id': "University of St. Michael’s",
      },{
        'name': "University of Sudbury",
        'id': "University of Sudbury",
      },{
        'name': "University of Toronto",
        'id': "University of Toronto",
      },{
        'name': "University of Trinity College",
        'id': "University of Trinity College",
      },{
        'name': "University of Waterloo",
        'id': "University of Waterloo",
      },{
        'name': "University of Western Ontario",
        'id': "University of Western Ontario",
      },{
        'name': "University of Windsor",
        'id': "University of Windsor",
      },{
        'name': "Victoria University",
        'id': "Victoria University",
      },{
        'name': "Wilfrid Laurier University",
        'id': "Wilfrid Laurier University",
      },{
        'name': "York University",
        'id': "York University",
      },{
        'name': "Other",
        'id': "Other",
      }]
   },
   {
    'name': "Alberta",
    'children': [{
        'name': "Alberta Bible College",
        'id': "Alberta Bible College",
      },
      {
        'name': "Alberta College of Art and Design",
        'id': "Alberta College of Art and Design",
      },
      {
        'name': "Ambrose University College",
        'id': "Ambrose University College",
      },
      {
        'name': "Athabasca University",
        'id': "Athabasca University",
      },
      {
        'name': "Augustana University College",
        'id': "Augustana University College",
      },
      {
        'name': "Bow Valley College",
        'id': "Bow Valley College",
      },
      {
        'name': "Canadian University College",
        'id': "Canadian University College",
      },
      {
        'name': "Concordia University - Alberta",
        'id': "Concordia University - Alberta",
      },
      {
        'name': "DeVry Institute of Technology",
        'id': "DeVry Institute of Technology",
      },
      {
        'name': "Grande Prairie Regional College",
        'id': "Grande Prairie Regional College",
      },
      {
        'name': "Grant MacEwan University",
        'id': "Grant MacEwan University",
      },
      {
        'name': "Keyano College",
        'id': "Keyano College",
      },
      {
        'name': "Lakeland College",
        'id': "Lakeland College",
      },
      {
        'name': "Lethbridge College",
        'id': "Lethbridge College",
      },
      {
        'name': "MacEwan University",
        'id': "MacEwan University",
      },
      {
        'name': "Medicine Hat College",
        'id': "Medicine Hat College",
      },
      {
        'name': "Mount Royal University",
        'id': "Mount Royal University",
      },
      {
        'name': "NAIT",
        'id': "NAIT",
      },
      {
        'name': "NorQuest College",
        'id': "NorQuest College",
      },
      {
        'name': "Northern Alberta Institute",
        'id': "Northern Alberta Institute",
      },
      {
        'name': "Northern Lakes College",
        'id': "Northern Lakes College",
      },
      {
        'name': "Olds College",
        'id': "Olds College",
      },
      {
        'name': "Portage College",
        'id': "Portage College",
      },
      {
        'name': "Red Deer College",
        'id': "Red Deer College",
      },
      {
        'name': "SAIT",
        'id': "SAIT",
      },
      {
        'name': "St. Mary's University College",
        'id': "St. Mary's University College",
      },
      {
        'name': "Taylor University",
        'id': "Taylor University",
      },
      {
        'name': "The King's University College",
        'id': "The King's University College",
      },
      {
        'name': "University of Alberta",
        'id': "University of Alberta",
      },
      {
        'name': "University of Calgary",
        'id': "University of Calgary",
      },
      {
        'name': "University of Lethbridge",
        'id': "University of Lethbridge",
      },
      {
        'name': "Other",
        'id': "Other",
      }]
   }
]
FOS = [
    "Agriculture",
    "Anthropology",
    "Archaeology",
    "Architecture and design",
    "Biology",
    "Business",
    "Chemistry",
    "Computer sciences",
    "Culinary arts",
    "Divinity",
    "Earth sciences",
    "Economics",
    "Education",
    "Engineering and technology",
    "Environmental studies and forestry",
    "Family and consumer science",
    "Geography",
    "History",
    "Human physical performance and recreation",
    "Interdisciplinary studies",
    "Journalism, media studies and communication",
    "Law",
    "Library and museum studies",
    "Linguistics and languages",
    "Literature",
    "Logic",
    "Mathematics",
    "Medicine",
    "Military sciences",
    "Performing arts",
    "Philosophy",
    "Physics",
    "Political science",
    "Psychology",
    "Public administration",
    "Religion",
    "Social work",
    "Sociology",
    "Space sciences",
    "Systems science",
    "Tourism", 
    "Transportation",
    "Visual arts"
]

feedback_report_emails=[
    'khedapt@gmail.com', 
    'edapt.developers@gmail.com', 
    'admin@techzillaindia.com'
    ]

VALID_STUDENT_DOCS = ["visa", "visa_img", "gic_cert", "passport"]
VALID_BUDDY_DOCS = ["primary", "secondary", "police_verification"]
UPDOWN_TOKEN = '3e52bbef-ecad-42bf-be65-67ddc5e43b67'