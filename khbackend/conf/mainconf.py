# Main configuration file for the complete flask application

from flask import Config

""" Common config can go here """

import os

UPLOAD_FOLDER = os.path.dirname(os.path.abspath(__file__))
UPLOAD_FOLDER = UPLOAD_FOLDER.replace("conf","uploads")

IMAGE_UPLOAD_FOLDER = os.path.dirname(os.path.abspath(__file__))
IMAGE_UPLOAD_FOLDER = IMAGE_UPLOAD_FOLDER.replace("conf","app")
IMAGE_UPLOAD_FOLDER = os.path.join(IMAGE_UPLOAD_FOLDER,'static','image')

class DevelopmentConfig(Config):
    URL = 'http://18.219.104.226:5000'
    MONGODB_DATABASE = 'khdb'
    MONGODB_HOST = 'localhost'
    MONGODB_PORT = 27017
    MONGODB_USERNAME = 'khuser'
    MONGODB_PASSWORD = 'khpass02'
    UPLOAD_FOLDER = UPLOAD_FOLDER
    IMAGE_UPLOAD_FOLDER = IMAGE_UPLOAD_FOLDER
    # TEXTLOCAL_APIKEY = 'sAsi+Sw5OEM-07vEC5VbftfEEJ3OnH3ADHhxsRbjw3'
    # smsala api key
    SMSALA_APIID = 'API72060304001'
    SMSALA_APIPASS = 'Walhekar12'
    SMSALA_SENDERID = 'smsala'
    # sendgrid api key
    SENDGRID_APIKEY = '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'
    # app SECRET_KEY
    SECRET_KEY = 'dummyTest44@$5%62^834^%$$#*'
    FCM_SERVER_KEY = 'AAAADicj7PU:APA91bGRLTa8ZzIJiKLqeU2IFVE_Haf2tyDXX1e1mvAC3kNDIER7xzeswf95I8VKwZlXqirm7ABn5KQkN0YOC50v72F1V5f7f6W0dnCRP_yOXlPE_fbh5lVf_EntAHs6u7uYHD4Gg6gs'
    INSTAMOJO_API_KEY = 'test_b8c3c25fd588d24d4d78d91611d'
    INSTAMOJO_AUTH_TOKEN = 'test_ba946b9a250e575976dd2d7000b'
    PAYPAL_ACCESSTOKEN = 'access_token$sandbox$ydqqrjr464wx9xk5$22c0b84da93eaa085a35103ab0fa5b4a' 
    PAYPAL_CLIENT_ID = 'ASz7k_TVuBFUmEjMaE_y6nDbieV4q5obKmx60LZs7KQt1hE5vgzizTjLyh1NTyMyFbMjb9EMVsGgfKiz'
    PAYPAL_CLIENT_SECRET = 'EDdxS8_U22ms1IaoJwpznDBPMs3O3ELid-hp7etaDhH3EcLxargYdEkWJAD_JzYeAaC7ePVf-T0o3g_T'
    ERROR_MESSAGE = 'Oops! Something went wrong. Please try again'
    INSTAMOJO_URL = 'https://test.instamojo.com/api/1.1/'

class ProductionConfig(Config):
    URL = 'http://prod.khedapt.com'
    MONGODB_DATABASE = 'khdb-prod'
    MONGODB_HOST = 'localhost'
    MONGODB_PORT = 27017
    MONGODB_USERNAME = 'khuser'
    MONGODB_PASSWORD = 'khpass02'
    UPLOAD_FOLDER = UPLOAD_FOLDER
    IMAGE_UPLOAD_FOLDER = IMAGE_UPLOAD_FOLDER
    # TEXTLOCAL_APIKEY = 'sAsi+Sw5OEM-07vEC5VbftfEEJ3OnH3ADHhxsRbjw3'
    # smsala api key
    SMSALA_APIID = 'API72060304001'
    SMSALA_APIPASS = 'Walhekar12'
    SMSALA_SENDERID = 'smsala'
    # sendgrid api key
    SENDGRID_APIKEY = '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'
    # app SECRET_KEY
    SECRET_KEY = 'dummyTest44@$5%62^834^%$$#*'
    FCM_SERVER_KEY = 'AAAADicj7PU:APA91bGRLTa8ZzIJiKLqeU2IFVE_Haf2tyDXX1e1mvAC3kNDIER7xzeswf95I8VKwZlXqirm7ABn5KQkN0YOC50v72F1V5f7f6W0dnCRP_yOXlPE_fbh5lVf_EntAHs6u7uYHD4Gg6gs'
    INSTAMOJO_API_KEY = 'test_b8c3c25fd588d24d4d78d91611d'
    INSTAMOJO_AUTH_TOKEN = 'test_ba946b9a250e575976dd2d7000b'
    ERROR_MESSAGE = 'Oops! Something went wrong. Please try again'
    INSTAMOJO_URL = 'https://test.instamojo.com/api/1.1/'
