# Python API project for KnowHassels Backend APIs

# Maintained in Python(Flask), MongoDb


# Start a virtualenv and Install app

```
$ virtualenv -p python3 khenv
$ cd khenv
$ source bin/activate
$ git clone <repo_url>
$ cd khbackend
$ pip install -r requirements.txt
```


# Start the app

```
$ python3 server.py

```
