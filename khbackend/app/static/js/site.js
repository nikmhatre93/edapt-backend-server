$(document).ready(function () {

	//create app wide globals
	window.$globals = {
		baseUrl: "http://localhost:5034",
		registerState: {},
		formData: {}
	}


	//app wide settings
	$(document).ajaxStart(function () {
		$("#ajaxLoader").show();
		$(document.body).css({
			'overflow': 'hidden'
		})
	});

	$(document).ajaxStop(function () {
		window.setTimeout(function () {
			$("#ajaxLoader").hide();
			$(document.body).css({
				'overflow': 'scroll'
			})
		}, 1000);
	});

	$("#logout-anchor").click(function () {
		document.cookie.split(";").forEach(function (c) {
			document.cookie = c.replace(/^ +/, "").replace(
				/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
		});
		window.location.href = "/";
	})

	//attach methods to events 

	// login: on enter 
	$('[name="username"], [name="password"]').keypress(function (e) {
		if (e.which == 13) {
			$("#login-btn").click();
			return false;    //<---- Add this line
		}
	});

	//login: login button click
	$("#login-btn").click(function () {
		if ($('[name="username"]').val() && $('[name="password"]').val()) {
			caller.makeLoginRequest({
				'username': $('[name="username"]').val(),
				'password': $('[name="password"]').val(),
				// 'accountId': $('[name="accountId"]').val(),
			});
		} else {
			// show error messaage
			$('#login-error').html("Please Enter a valid Username and Password");
			$('#login-error').removeClass("hidden");
		}
	});

	//add group
	$("#add-group-btn").click(function () {
		validateResponse = caller.validateForm($('#add-group-form'));
		if (validateResponse.status) {
			caller.createGroup({
				'data': validateResponse.data
			}, function () {
				console.log("Performing Callback");

			});

		}
	});

	//add parent
	$("#add-parent-btn").click(function () {
		validateResponse = caller.validateForm($('#add-parent-form'));
		if (validateResponse.status) {
			var interests = $("[name='interests']").val();
			var allInterests = [];

			for (i = 0; i < interests.length; i++) {
				allInterests.push({
					"name": interests[i]
				})
			}
			validateResponse.data['interests'] = allInterests;

			validateResponse.data['groupTitle'] = $("[data-group-id='" + validateResponse.data['groupId'] + "']").data('group-title');

			console.log(validateResponse.data);
			caller.addParent({
				'data': validateResponse.data
			}, function () {
				console.log("Performing Callback");
			});
		}
	});


	//add expert
	$("#add-expert-btn").click(function () {
		validateResponse = caller.validateForm($('#add-expert-form'));
		if (validateResponse.status) {
			var interests = $("[name='interests']").val();
			var allInterests = [];

			for (i = 0; i < interests.length; i++) {
				allInterests.push({
					"name": interests[i]
				})
			}
			validateResponse.data['interests'] = allInterests;

			var ageGroup = $("[name='ageGroup']").val();
			var allGroups = [];

			for (i = 0; i < ageGroup.length; i++) {
				allGroups.push({
					"name": ageGroup[i]
				})
			}
			validateResponse.data['ageGroup'] = allGroups;


			console.log(validateResponse.data);
			caller.addExpert({
				'data': validateResponse.data
			}, function () {
				console.log("Performing Callback");
			});
		}
	});


	//add question
	$("#add-question-btn").click(function () {
		validateResponse = caller.validateForm($('#add-question-form'));
		if (validateResponse.status) {

			var tags = $("[name='tags']").val();
			var alltags = [];

			for (i = 0; i < tags.length; i++) {
				alltags.push({
					"name": tags[i]
				})
			}
			validateResponse.data['tags'] = alltags;

			var ageGroup = $("[name='ageGroup']").val();
			var allGroups = [];

			for (i = 0; i < ageGroup.length; i++) {
				allGroups.push({
					"name": ageGroup[i]
				})
			}
			validateResponse.data['ageGroup'] = allGroups;

			validateResponse.data['groupTitle'] = $("[data-group-id='" + validateResponse.data['groupId'] + "']").data('group-title');

			validateResponse.data['user'] = {
				'id': "583d570b0942edd1c3f26ff0",
				'name': 'Admin',
				'role': 'admin',
				'isAnon': false,
				'pic': '/static/image/logo.jpg'
			};


			console.log(validateResponse.data);
			caller.addQuestion({
				'data': validateResponse.data
			}, function () {
				console.log("Performing Callback");
			});
		}
	});


	//edit parent
	$("#edit-parent-btn").click(function () {
		validateResponse = caller.validateForm($('#edit-parent-form'));
		if (validateResponse.status) {
			var interests = $("[name='edit-interests']").val();
			var allInterests = [];

			for (i = 0; i < interests.length; i++) {
				allInterests.push({
					"name": interests[i]
				})
			}
			validateResponse.data['edit-interests'] = allInterests;



			console.log(validateResponse.data);

			var data = {};
			for (key in validateResponse.data) {
				newkey = key.replace('edit-', '');
				data[newkey] = validateResponse.data[key]
			}

			console.log(data);

			caller.editParent({
				'data': data
			}, function () {
				console.log("Performing Callback");
			});
		}
	});


	//edit expert
	$("#edit-expert-btn").click(function () {
		validateResponse = caller.validateForm($('#edit-expert-form'));
		if (validateResponse.status) {
			var interests = $("[name='edit-interests']").val();
			var allInterests = [];

			for (i = 0; i < interests.length; i++) {
				allInterests.push({
					"name": interests[i]
				})
			}
			validateResponse.data['edit-interests'] = allInterests;

			var ageGroup = $("[name='edit-ageGroup']").val();
			var allGroups = [];

			for (i = 0; i < ageGroup.length; i++) {
				allGroups.push({
					"name": ageGroup[i]
				})
			}
			validateResponse.data['edit-ageGroup'] = allGroups;

			console.log(validateResponse.data);

			var data = {};
			for (key in validateResponse.data) {
				newkey = key.replace('edit-', '');
				data[newkey] = validateResponse.data[key]
			}

			console.log(data);

			caller.editExpert({
				'data': data
			}, function () {
				console.log("Performing Callback");
			});
		}
	});

	$("#add-service-btn").click(function () {
		validateResponse = caller.validateForm($('#add-service-form'), ["url"]);
		if (validateResponse.status) {
			var subcategory = $("[name='subcategory']").val();
			var allSubcategory = [];

			for (i = 0; i < subcategory.length; i++) {
				allSubcategory.push({
					"name": subcategory[i]
				})
			}
			validateResponse.data['subcategory'] = allSubcategory;

			var category = $("[name='category']").val();
			var allCategory = [];

			for (i = 0; i < category.length; i++) {
				allCategory.push({
					"name": category[i]
				})
			}
			validateResponse.data['category'] = allCategory;


			console.log(validateResponse.data);
			caller.addService({
				'data': validateResponse.data
			}, function () {
				console.log("Performing Callback");
			});
		}
	});

	$("#edit-service-btn").click(function () {
		console.log("Entered in Update Button click");
		validateResponse = caller.validateForm($('#edit-service-form'));
		if (validateResponse.status) {
			var subcategory = $("[name='edit-subcategory']").val();
			var allSubcategory = [];

			for (i = 0; i < subcategory.length; i++) {
				allSubcategory.push({
					"name": subcategory[i]
				})
			}
			validateResponse.data['subcategory'] = allSubcategory;

			var category = $("[name='edit-category']").val();
			var allCategory = [];

			for (i = 0; i < category.length; i++) {
				allCategory.push({
					"name": category[i]
				})
			}
			validateResponse.data['category'] = allCategory;


			console.log(validateResponse.data);

			var data = {};
			for (key in validateResponse.data) {
				newkey = key.replace('edit-', '');
				data[newkey] = validateResponse.data[key]
			}

			caller.editService({
				'data': data
			}, function () {
				console.log("Performing Callback");
			});
		}
	});




	//auto calculate the total amount
	$(document.body).on('keyup', '[name="totalQuantity"]', function () {
		var totalAmount = $('[name="totalQuantity"]').val() * $('[name="ratePerUnit"]').val()
		$('[name="totalAmount"]').val(totalAmount);
	});

	//define caller functions to be attached to the events
	var caller = {

		makeLoginRequest: function (params) {
			console.log(params);
			$.ajax({
				'url': "/admin/login",
				'type': "POST",
				'data': JSON.stringify(params),
				'contentType': "application/json",
				success: function (response) {
					console.log(response);
					if (response['status']) {
						$.cookie("auth_token", response["data"]['access_token']);
						$('#login-error').html("");
						$('#login-error').addClass("hidden");
						//call the dashboard end point with the auth_token saved in cookies
						window.location.href = "/admin/dashboard";

					} else {
						$('#login-error').html(response['message']);
						$('#login-error').removeClass("hidden");
					}

				},
				failure: function (response) {
					console.log(response);
				}
			});
		},

		createGroup: function (params, callback) {
			/*
				params : {
					data
				}
			*/
			console.log(params);

			$.ajax({
				'url': "/group",
				'type': "POST",
				'data': JSON.stringify(params),
				'contentType': "application/json",
				beforeSend: function (xhr) {
					xhr.setRequestHeader("Authorization", $.cookie("auth_token"));
				},
				success: function (response) {
					console.log(response);
					if (response.status) {
						swal("Done", "Success", "success");
						window.location.reload();
					} else {
						swal("Error", response.message, "error");
					}
				},
				failure: function (response) {
					console.log(response);
				}
			});
		},

		addParent: function (params, callback) {
			/*
				params : {
					data
				}
			*/
			console.log(params);

			$.ajax({
				'url': "/parent",
				'type': "POST",
				'data': JSON.stringify(params),
				'contentType': "application/json",
				beforeSend: function (xhr) {
					xhr.setRequestHeader("Authorization", $.cookie("auth_token"));
				},
				success: function (response) {
					console.log(response);
					if (response.status) {
						swal("Done", "Success", "success");
						window.location.reload();
					} else {
						swal("Error", response.message, "error");
					}
				},
				failure: function (response) {
					console.log(response);
				}
			});
		},

		addExpert: function (params, callback) {
			/*
				params : {
					data
				}
			*/
			console.log(params);

			$.ajax({
				'url': "/expert",
				'type': "POST",
				'data': JSON.stringify(params),
				'contentType': "application/json",
				beforeSend: function (xhr) {
					xhr.setRequestHeader("Authorization", $.cookie("auth_token"));
				},
				success: function (response) {
					console.log(response);
					if (response.status) {
						swal("Done", "Success", "success");
						window.location.reload();
					} else {
						swal("Error", response.message, "error");
					}
				},
				failure: function (response) {
					console.log(response);
				}
			});
		},

		addQuestion: function (params, callback) {
			/*
				params : {
					data
				}
			*/
			console.log(params);

			$.ajax({
				'url': "/question",
				'type': "POST",
				'data': JSON.stringify(params),
				'contentType': "application/json",
				beforeSend: function (xhr) {
					xhr.setRequestHeader("Authorization", $.cookie("auth_token"));
				},
				success: function (response) {
					console.log(response);
					if (response.status) {
						swal("Done", "Success", "success");
						window.location.reload();
					} else {
						swal("Error", response.message, "error");
					}
				},
				failure: function (response) {
					console.log(response);
				}
			});
		},

		editParent: function (params, callback) {
			/*
				params : {
					data
				}
			*/
			console.log(params);

			$.ajax({
				'url': "/parent/" + window.current_parent.id,
				'type': "PUT",
				'data': JSON.stringify(params),
				'contentType': "application/json",
				beforeSend: function (xhr) {
					xhr.setRequestHeader("Authorization", $.cookie("auth_token"));
				},
				success: function (response) {
					console.log(response);
					if (response.status) {
						swal({
							title: "Profile Edited successfully",
							type: "success",
							showCancelButton: false,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Okay!",
							closeOnConfirm: false
						},
							function () {
								window.location.reload();
							});
					} else {
						swal("Error", response.message, "error");
					}
				},
				failure: function (response) {
					console.log(response);
				}
			});
		},

		editExpert: function (params, callback) {
			/*
				params : {
					data
				}
			*/
			console.log(params);

			$.ajax({
				'url': "/expert/" + window.current_expert.id,
				'type': "PUT",
				'data': JSON.stringify(params),
				'contentType': "application/json",
				beforeSend: function (xhr) {
					xhr.setRequestHeader("Authorization", $.cookie("auth_token"));
				},
				success: function (response) {
					console.log(response);
					if (response.status) {
						swal({
							title: "Profile Edited successfully",
							type: "success",
							showCancelButton: false,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Okay!",
							closeOnConfirm: false
						},
							function () {
								window.location.reload();
							});
					} else {
						swal("Error", response.message, "error");
					}
				},
				failure: function (response) {
					console.log(response);
				}
			});
		},

		addService: function (params, callback) {
			/*
				params : {
					data
				}
			*/
			console.log(params);

			$.ajax({
				'url': "/services",
				'type': "POST",
				'data': JSON.stringify(params),
				'contentType': "application/json",
				beforeSend: function (xhr) {
					xhr.setRequestHeader("Authorization", $.cookie("auth_token"));
				},
				success: function (response) {
					console.log(response);
					if (response.status) {
						swal("Done", "Success", "success");
						window.location.reload();
					} else {
						swal("Error", response.message, "error");
					}
				},
				failure: function (response) {
					console.log(response);
				}
			});
		},

		editService: function (params, callback) {
			/*
				params : {
					data
				}
			*/
			console.log(params);

			$.ajax({
				'url': "/services/" + window.current_service.id,
				'type': "PUT",
				'data': JSON.stringify(params),
				'contentType': "application/json",
				beforeSend: function (xhr) {
					xhr.setRequestHeader("Authorization", $.cookie("auth_token"));
				},
				success: function (response) {
					console.log(response);
					if (response.status) {
						swal({
							title: "Service Edited successfully",
							type: "success",
							showCancelButton: false,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Okay!",
							closeOnConfirm: false
						},
							function () {
								window.location.reload();
							});
					} else {
						swal("Error", response.message, "error");
					}
				},
				failure: function (response) {
					console.log(response);
				}
			});
		},

		validateForm: function (form, exceptions) {
			var datajson = {};
			var viewArr = form.serializeArray();

			var isValid = true;

			if (exceptions == undefined || exceptions == null) {
				exceptions = [];
			}

			for (var i in viewArr) {

				name = viewArr[i].name;

				//if name not in exceptions than carry out the validation
				if (exceptions.indexOf(name) < 0) {

					value = viewArr[i].value.trim()
					datajson[name] = value;

					if (value == "") {
						$('[name="' + name + '"]').addClass('form-error');
						isValid = false;
					} else {
						$('[name="' + name + '"]').removeClass('form-error');
					}
				} else {
					value = viewArr[i].value.trim()
					datajson[name] = value;
				}

			}
			console.log(datajson);
			if (!isValid) {
				return {
					status: false
				};
			}

			return {
				status: true,
				data: datajson
			};
		},

		renderTemplate: function (template, data, result, callback) {
			console.log(data);
			var template = $(template).html();
			var rendered = Mustache.render(template, {
				'data': data['data'],
				'isEmpty': data['isEmpty']
			});
			$(result).html(rendered);
			callback();
		}

	}


})