from flask import Flask, jsonify, Response, request, send_from_directory, render_template
from flask_restful import Resource, Api, reqparse
from datetime import datetime, timedelta
import json
import uuid
import os
from bson import json_util
from bson.objectid import ObjectId
from copy import deepcopy

from libs.decorators import *
from libs import tokenize
import conf.constants as C

from app import app, db, logger

from ..models.user_models import *
from ..models.service_models import Service, ServiceTask, Package
from .custom_utils import ResponseUtil
from libs.email_notification import *
from libs.push_notification import *
from libs.payments import *
from .generic_controller import *

def send_user_email(response, first_name, email, for_buddy=False):
    try:
        dict_response = json.loads(response.data.decode())
        app.logger.error(dict_response)
        # special case for email verificaiton
        if dict_response["status"] or (
            not dict_response["status"] and  'deactivated' in dict_response['message']
            ):
            app.logger.error("Using this - {} - {}".format(first_name, email))
            
            if for_buddy:
                email_obj = Email_(deepcopy(C.EMAIL_TEMPLATES["signup_buddy"]))
            else:
                email_obj = Email_(deepcopy(C.EMAIL_TEMPLATES["signup_student"]))

            app.logger.error("Generating token with first_name {} email {}".format(first_name, email))

            token = generate_token(email)

            url = C.APP_BASE_URL + "/user/account/verify/"+token
            app.logger.error(token)

            email_obj.template["message"] = email_obj.template["message"].format(FIRST_NAME=first_name, URL=url)

            app.logger.error(email_obj.template["message"])

            if email not in ["","",None]:
                email_params = {
                    "to_email": email
                }
                email_response = email_obj.send_mail(email_params)
                del email_obj
                app.logger.error(email_response)

    except Exception as e:
        app.logger.error(e)

class UserRegister(Resource):

    def post(self):
        try:
            json_data = request.get_json(force=True)
            first_name = json_data['first_name']
            email = json_data['email']

            print(json_data)
            if str(json_data['role_type']).upper() == "BUDDY":
                response = User().create_one(json_data, buddy_register=True)
                if json_data['login_type'] == "knowhassles":
                    send_user_email(response, first_name, email, for_buddy=True)
            elif str(json_data['role_type']).upper() == "STUDENT":
                response = User().create_one(json_data, buddy_register=False)
                if json_data['login_type'] == "knowhassles":
                    send_user_email(response, first_name, email, for_buddy=False)
            else:
                return ResponseUtil(False, 403).message_response("Invalid Role Type")

            return response
        except Exception as e:
            app.logger.error(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")

    @allowed_api_roles([C.APP_ROLES.get('STUDENT')])
    def put(self, **kwargs):
        user_id = kwargs['payload']['user_id']

        try:
            json_data = request.get_json(force=True)
        except Exception as e:
            return ResponseUtil(False, 403).json_message_response("Invalid json request")

        response = User().become_mentor(user_id, json_data)

        return response

class UserLogin(Resource):

    def post(self):
        json_data = request.get_json(force=True)
        response = User().validate_user(json_data)
        return response


class UserAccount(Resource):

    @allowed_api_roles([C.APP_ROLES.get('ADMIN'), C.APP_ROLES.get('STUDENT'),
                        C.APP_ROLES.get('BUDDY'), C.APP_ROLES.get('EVENTMGR')])
    def get(self, **kwargs):
        user_id = kwargs['payload']['user_id']

        response = User().get_one(user_id)

        # print(response)

        response_percentage = User().get_percentage(
            response['data'], kwargs['payload']['login_as'].upper())

        if response_percentage['status']:
            percentage = response_percentage['data']['percentage']
            response['data']['percentage'] = percentage

        if not response['status']:
            ResponseUtil(False, 403).json_message_response(response['message'])
        return ResponseUtil(True, 200).json_data_response(response['data'])

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def post(self, pic=None, **kwargs):
        try:
            if pic == 'profilepicture':
                files = request.files.to_dict()
                return User().update_user_profile_picture(files, kwargs['payload']['user_id'])

            else:
                json_data = request.get_json(force=True)
                json_data['user_id'] = kwargs['payload']['user_id']

                if "is_active" in json_data:
                    json_data.pop("is_active")

                return User().update_user_meta(json_data)

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def delete(self, **kwargs):
        try:
            user_id = kwargs['payload']['user_id']
            
            try:
                User.objects(_id=user_id).update_one(is_deleted=True)
                return ResponseUtil(True, 200).message_response("Your account has been deleted. We would be happy to have you back again!")
            except Exception as e:
                app.logger.error("Error while deleting user profile {}".format(str(e)))
                return ResponseUtil(False, 403).message_response("Something went wrong!")

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")

class UserAccount2(Resource):
    def post(self, pic=None, **kwargs):

        print(' json_data ==========',request.form.to_dict(flat=False))
        print(request.files.to_dict())
       
        try:
            
            files = request.files.to_dict()
            email = request.form.to_dict(flat=False)['email'][0]
            user_id = User().get_user_id(email=email)[0]['_id']['$oid']
            # print(user_id)
            User().update_user_profile_picture(files, user_id)
            return ResponseUtil(True, 200).message_response("...")

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")

class UpdatePassword(Resource):
    @allowed_api_roles([C.APP_ROLES.get('STUDENT'),
                        C.APP_ROLES.get('BUDDY')])
    def post(self, **kwargs):
        try:
            user_id = kwargs['payload']['user_id']
            json_data = request.get_json(force=True)
            
            if all(x in json_data.keys() for x in ['old_password', 'new_password']):
                return User().update_password(user_id, json_data)
            else:
                raise Exception

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")


class Dashboard(Resource):

    @allowed_api_roles([C.APP_ROLES.get('ADMIN'), C.APP_ROLES.get('STUDENT'),
                        C.APP_ROLES.get('BUDDY'), C.APP_ROLES.get('EVENTMGR')])
    def get(self, **kwargs):
        user_id = kwargs['payload']['user_id']

        if "login_as" in kwargs["payload"] and kwargs["payload"]["login_as"] == "BUDDY":
            response = Buddy().get_dashboard_data(
                kwargs["payload"]["user_id"], Package)
        elif "login_as" in kwargs["payload"] and kwargs["payload"]["login_as"] == "STUDENT":
            response = Student().get_dashboard_data(
                kwargs["payload"]["user_id"])

            if response["status"]:
                response_percentage = User().get_percentage(
                    response['data']['user'], kwargs['payload']['login_as'].upper())

                if response_percentage['status']:
                    percentage = response_percentage['data']['percentage']
                    response['data']['user']['percentage'] = percentage

        else:
            return ResponseUtil(False, 403).message_response("Failed to get current login type info")

        return response
        # return User().get_dashboard_data(user_id)


class FCMUpdate(Resource):
    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def post(self, **kwargs):
        try:
            json_data = request.get_json(force=True)
            user_id = kwargs['payload']['user_id']

            # remove other duplicate fcm tokens
            User.objects(
                _id__ne=user_id, fcm_token=json_data["fcm_token"]
            ).update(fcm_token="")

            # update new fcm token
            User.objects(_id=user_id).update_one(
                fcm_token=json_data["fcm_token"], deviceType=json_data['deviceType'])

            return ResponseUtil(True, 200).message_response("FCM Token Updated successfully")

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def delete(self, **kwargs):
        try:
            json_data = request.get_json(force=True)
            user_id = kwargs['payload']['user_id']
            User.objects(_id=user_id).update_one(
                fcm_token=""
            )
            return ResponseUtil(True, 200).message_response("FCM Token Updated successfully")

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")


class KhStudent(Resource):

    @allowed_api_roles([C.APP_ROLES.get('ADMIN'), C.APP_ROLES.get('STUDENT'),
                        C.APP_ROLES.get('BUDDY'), C.APP_ROLES.get('EVENTMGR')])
    def get(self, user_id=None, **kwargs):

        if user_id:
            return User().get_one(user_id, service_task_model=ServiceTask)
        else:
            self_id = kwargs['payload']['user_id']
            number_of_results = request.args.get("count")
            search_keyword = request.args.get("search_keyword", None)

            if not number_of_results:
                number_of_results = "0"

            if number_of_results and str(number_of_results).isdigit():

                # print(number_of_results)
                # print(kwargs['payload'])

                if "login_as" in kwargs['payload'] and kwargs['payload']["login_as"] == "BUDDY":
                    # if C.APP_ROLES.get('BUDDY') in kwargs['payload']['role']:
                    # get all students where I'm serving

                    return Student().get_all(n=int(number_of_results), self_id=self_id, self_role="buddy", order_by="created", reverse_sort=True, search_keyword=search_keyword)

                elif "login_as" in kwargs['payload'] and kwargs['payload']["login_as"] == "STUDENT":
                    # elif C.APP_ROLES.get('STUDENT') in kwargs['payload']['role']:
                    # get all students travelling with me

                    return Student().get_all(n=int(number_of_results), self_id=self_id, self_role="student", order_by="created", reverse_sort=True, search_keyword=search_keyword)

                else:
                    return Student().get_all()

            else:
                return ResponseUtil(False, 400).message_response("Incomplete/invalid url parameter 'count'")

    @allowed_api_roles([C.APP_ROLES.get('STUDENT')])
    def post(self, user_id=None, **kwargs):
        try:
            json_data = request.get_json(force=True)
            json_data['user_id'] = kwargs['payload']['user_id']

            return Student()._update(json_data)

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")


#New Dhruv API for searching students
class SearchStudents(Resource):

    @allowed_api_roles([C.APP_ROLES.get('ADMIN'), C.APP_ROLES.get('STUDENT'),
                        C.APP_ROLES.get('BUDDY'), C.APP_ROLES.get('EVENTMGR')])
    def get(self, user_id=None, **kwargs):

        if user_id:
            return User().get_one(user_id, service_task_model=ServiceTask, current_user_id= kwargs['payload']['user_id'])
        else:
            self_id = kwargs['payload']['user_id']
            search_keyword = request.args.get("search_keyword", None)

            if "login_as" in kwargs['payload'] and kwargs['payload']["login_as"] == "BUDDY":
                return Student().get_alls_search_student(
                    
                    self_id=self_id, 
                    self_role="buddy", 
                    order_by="created", 
                    reverse_sort=True, 
                    search_keyword=search_keyword,
                    
                )

            elif "login_as" in kwargs['payload'] and kwargs['payload']["login_as"] == "STUDENT":
                return Student().get_alls_search_student(
                    
                    self_id=self_id, 
                    self_role="student", 
                    order_by="created", 
                    reverse_sort=True, 
                    search_keyword=search_keyword,
                    
                )
            else:
                return Student().get_alls_search_student()


class KhStudents(Resource):

    @allowed_api_roles([C.APP_ROLES.get('ADMIN'), C.APP_ROLES.get('STUDENT'),
                        C.APP_ROLES.get('BUDDY'), C.APP_ROLES.get('EVENTMGR')])
    def get(self, user_id=None, **kwargs):

        if user_id:
            return User().get_one(user_id, service_task_model=ServiceTask, current_user_id= kwargs['payload']['user_id'])
        else:
            self_id = kwargs['payload']['user_id']
            number_of_results = request.args.get("count")
            offset = request.args.get("offset")
            # print('******************************************************',request.args.get('is_province'))
            is_province = True if request.args.get('is_province') == 'true' else False
            # print('******************************************************',is_province)
            search_keyword = request.args.get("search_keyword", None)

            if not number_of_results:
                number_of_results = "0"

            if number_of_results and str(number_of_results).isdigit():

                # print(number_of_results)
                # print(kwargs['payload'])

                if "login_as" in kwargs['payload'] and kwargs['payload']["login_as"] == "BUDDY":
                    # if C.APP_ROLES.get('BUDDY') in kwargs['payload']['role']:
                    # get all students where I'm serving

                    return Student().get_alls(n=int(number_of_results), self_id=self_id, self_role="buddy", order_by="created", reverse_sort=True, search_keyword=search_keyword, offset=offset, is_province=is_province)

                elif "login_as" in kwargs['payload'] and kwargs['payload']["login_as"] == "STUDENT":
                    # elif C.APP_ROLES.get('STUDENT') in kwargs['payload']['role']:
                    # get all students travelling with me

                    return Student().get_alls(n=int(number_of_results), self_id=self_id, self_role="student", order_by="created", reverse_sort=True, search_keyword=search_keyword, offset=offset, is_province=is_province)

                else:
                    return Student().get_alls()

            else:
                return ResponseUtil(False, 400).message_response("Incomplete/invalid url parameter 'count'")

    @allowed_api_roles([C.APP_ROLES.get('STUDENT')])
    def post(self, user_id=None, **kwargs):
        try:
            json_data = request.get_json(force=True)
            json_data['user_id'] = kwargs['payload']['user_id']

            return Student()._update(json_data)

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")



class KhBuddy(Resource):

    @allowed_api_roles([C.APP_ROLES.get('ADMIN'), C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY'), C.APP_ROLES.get('EVENTMGR')])
    def get(self, user_id=None, **kwargs):

        self_id = kwargs['payload']['user_id']
        number_of_results = request.args.get("count")
        search_keyword = request.args.get("search_keyword", None)

        if not number_of_results:
            number_of_results = "0"

        if user_id:
            single_buddy_response = Buddy().get_all(
                n=1, user_id=user_id, self_id=self_id, self_role="student", service_model=Service, get_one=True, package_model=Package, search_keyword=search_keyword)
            if single_buddy_response["status"]:
                return ResponseUtil(True, 200).data_response(single_buddy_response["data"]["buddies"][0])
            else:
                return single_buddy_response
        else:

            if number_of_results and str(number_of_results).isdigit():

                if "login_as" in kwargs['payload'] and kwargs['payload']["login_as"] == "BUDDY":
                    # if C.APP_ROLES.get('BUDDY') in kwargs['payload']['role']:
                    return Buddy().get_all(n=int(number_of_results), self_id=self_id, self_role="buddy", service_model=Service, order_by="created", reverse_sort=True, search_keyword=search_keyword)
                elif "login_as" in kwargs['payload'] and kwargs['payload']["login_as"] == "STUDENT":
                    # elif C.APP_ROLES.get('STUDENT') in
                    # kwargs['payload']['role']:
                    return Buddy().get_all(n=int(number_of_results), self_id=self_id, self_role="student", service_model=Service, package_model=Package, order_by="created", reverse_sort=True, search_keyword=search_keyword)

                else:
                    return Buddy().get_all()
            else:
                return ResponseUtil(False, 400).message_response("Incomplete/invalid url parameter")

    @allowed_api_roles([C.APP_ROLES.get('BUDDY')])
    def post(self, user_id=None, **kwargs):
        try:
            json_data = request.get_json(force=True)
            json_data['user_id'] = kwargs['payload']['user_id']

            return Buddy()._update(json_data)

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")

    @allowed_api_roles([C.APP_ROLES.get('STUDENT')])
    def put(self, user_id=None, **kwargs):
        try:
            if user_id:
                json_data = request.get_json(force=True)
                json_data['writer'] = kwargs['payload']['user_id']
                json_data['user_id'] = user_id

                return Review().add_one(params=json_data)
            else:
                return ResponseUtil(False, 403).message_response("Invalid User ID")

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")

class SearchBuddies(Resource):

    @allowed_api_roles([C.APP_ROLES.get('ADMIN'), C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY'), C.APP_ROLES.get('EVENTMGR')])
    def get(self, user_id=None, **kwargs):

        self_id = kwargs['payload']['user_id']
        search_keyword = request.args.get("search_keyword", None)

        if user_id:
           
            single_buddy_response = Buddy().get_alls_search_buddy(
                n=1, user_id=user_id, self_id=self_id, self_role="student", service_model=Service, get_one=True, package_model=Package, search_keyword=search_keyword)
            if single_buddy_response["status"]:
                return ResponseUtil(True, 200).data_response(single_buddy_response["data"]["buddies"][0])
            else:
                return single_buddy_response
        else:
            if "login_as" in kwargs['payload'] and kwargs['payload']["login_as"] == "BUDDY":
                return Buddy().get_alls_search_buddy( 
                        self_id=self_id, 
                        self_role="buddy", 
                        service_model=Service, 
                        order_by="created", 
                        reverse_sort=True, 
                        search_keyword=search_keyword,
                    )
            elif "login_as" in kwargs['payload'] and kwargs['payload']["login_as"] == "STUDENT":
                return Buddy().get_alls_search_buddy(
                        self_id=self_id, 
                        self_role="student", 
                        service_model=Service, 
                        package_model=Package, 
                        order_by="created", 
                        reverse_sort=True, 
                        search_keyword=search_keyword,
                    )

            else:
                return Buddy().get_alls_search_buddy()

class KhBuddies(Resource):

    @allowed_api_roles([C.APP_ROLES.get('ADMIN'), C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY'), C.APP_ROLES.get('EVENTMGR')])
    def get(self, user_id=None, **kwargs):

        self_id = kwargs['payload']['user_id']
        # print(self_id)
        number_of_results = request.args.get("count")
        offset = request.args.get("offset")
        is_province = True if request.args.get('is_province') == 'true' else False
        search_keyword = request.args.get("search_keyword", None)

        if not number_of_results:
            number_of_results = "0"

        if user_id:
           
            single_buddy_response = Buddy().get_alls(
                n=1, user_id=user_id, self_id=self_id, self_role="student", service_model=Service, get_one=True, package_model=Package, search_keyword=search_keyword)
            if single_buddy_response["status"]:
                return ResponseUtil(True, 200).data_response(single_buddy_response["data"]["buddies"][0])
            else:
                return single_buddy_response
        else:
            print('2')
            if number_of_results and str(number_of_results).isdigit():

                if "login_as" in kwargs['payload'] and kwargs['payload']["login_as"] == "BUDDY":
                    print('1')
                    # if C.APP_ROLES.get('BUDDY') in kwargs['payload']['role']:
                    return Buddy().get_alls(n=int(number_of_results), self_id=self_id, self_role="buddy", service_model=Service, order_by="created", reverse_sort=True, search_keyword=search_keyword, offset=offset, is_province=is_province)
                elif "login_as" in kwargs['payload'] and kwargs['payload']["login_as"] == "STUDENT":
                    print('2')
                    # elif C.APP_ROLES.get('STUDENT') in
                    # kwargs['payload']['role']:
                    return Buddy().get_alls(n=int(number_of_results), self_id=self_id, self_role="student", service_model=Service, package_model=Package, order_by="created", reverse_sort=True, search_keyword=search_keyword, offset=offset, is_province=is_province)

                else:
                    print('3')
                    return Buddy().get_alls()
            else:
                return ResponseUtil(False, 400).message_response("Incomplete/invalid url parameter")


    @allowed_api_roles([C.APP_ROLES.get('BUDDY')])
    def post(self, user_id=None, **kwargs):
        try:
            json_data = request.get_json(force=True)
            json_data['user_id'] = kwargs['payload']['user_id']

            return Buddy()._update(json_data)

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")

    @allowed_api_roles([C.APP_ROLES.get('STUDENT')])
    def put(self, user_id=None, **kwargs):
        try:
            if user_id:
                json_data = request.get_json(force=True)
                json_data['writer'] = kwargs['payload']['user_id']
                json_data['user_id'] = user_id

                return Review().add_one(params=json_data)
            else:
                return ResponseUtil(False, 403).message_response("Invalid User ID")

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")



class StudentTravel(Resource):

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY'), C.APP_ROLES.get('ADMIN')])
    def get(self, doc_type=None, user_id=None, **kwargs):
        try:
            if not user_id:
                user_id = kwargs["payload"]["user_id"]

            if doc_type:

                response = TravelDocs().get_one(user_id=user_id)

                if response["status"]:
                    files = response["data"]

                    if doc_type not in files:
                        return ResponseUtil(False, 403).message_response("Document not uploaded")

                    fileName = files[doc_type]

                    # return send_from_directory(app.config.get("UPLOAD_FOLDER")+"/"+doc_type, files[doc_type], as_attachment=True)
                    return ResponseUtil(True, 200).json_data_response({
                        'fileName': fileName
                    })
                else:
                    return response

            else:
                return ResponseUtil(False, 403).message_response("Document type required")

        except Exception as e:
            app.logger.error(e)
            return ResponseUtil(False, 403).message_response("Exception {}".format(e))

    @allowed_api_roles([C.APP_ROLES.get('STUDENT')])
    def post(self, **kwargs):
        try:
            user_id = kwargs["payload"]["user_id"]
            files = request.files.to_dict()

            if list(files.keys()):
                doc_type = list(files.keys())[0]
                params = {
                    doc_type: ".".join([str(uuid.uuid4()), files[doc_type].content_type.split('/')[1]])
                }

                # dir_to_save = os.path.join(
                #     app.config['UPLOAD_FOLDER'], doc_type)

                # if not os.path.exists(dir_to_save):
                #     os.makedirs(dir_to_save)

                # files[doc_type].save(os.path.join(
                #     dir_to_save, params[doc_type]))

                dir_to_save = os.path.join(
                    app.config['IMAGE_UPLOAD_FOLDER'], doc_type)

                if not os.path.exists(dir_to_save):
                    os.makedirs(dir_to_save)

                # adding a fake add to the statics folder
                files[doc_type].save(os.path.join(
                    dir_to_save, params[doc_type]))

                response = TravelDocs().add(user_id, params)

                return response

            else:
                return ResponseUtil(False, 403).message_response("No files found")

        except Exception as e:
            print(e)
            app.logger.error(
                "Exception while buddy verification: {}".format(e))
            return ResponseUtil(False, 403).message_response("Invalid request. Exception: {}".format(e))


class BuddyVerify(Resource):

    def allowed_file(self, filename):
        return '.' in filename and filename.rsplit('.', 1)[1].lower() in C.ALLOWED_EXTENSIONS

    # @allowed_api_roles([C.APP_ROLES.get('BUDDY'), C.APP_ROLES.get('ADMIN')])
    def get(self, doc_type=None, user_id=None, **kwargs):
        try:
            if not user_id:
                user_id = kwargs["payload"]["user_id"]

            if doc_type:
                mapping = {"primary": "primary_id", "secondary": "secondary_id",
                           "police_verification": "police_verification"}
                response = VerificationDocs().get_one(user_id=user_id)
                if response["status"]:
                    files = response["data"]

                    return send_from_directory(app.config.get("UPLOAD_FOLDER")+"/"+doc_type, files[mapping[doc_type]], as_attachment=True)

                else:
                    return response

            else:
                return ResponseUtil(False, 403).message_response("Document type required")

        except Exception as e:
            app.logger.error(e)
            return ResponseUtil(False, 403).message_response("Exception {}".format(e))

    @allowed_api_roles([C.APP_ROLES.get('BUDDY')])
    def post(self, doc_type=None, **kwargs):
        try:
            user_id = kwargs["payload"]["user_id"]
            files = request.files.to_dict()
            # print(user_id)
            # print(files)
            app.logger.error("files keys:" + str(list(files.keys())))

            mapping = {"primary": "primary_id",
                       "secondary": "secondary_id",
                       "police_verification": "police_verification"
                       }

            if doc_type in mapping.keys() and mapping[doc_type] in files.keys():

                # filenames = {
                #     "primary": ".".join([str(uuid.uuid4()), files["primary_id"].content_type.split('/')[1]]),
                #     "secondary": ".".join([str(uuid.uuid4()), files["secondary_id"].content_type.split('/')[1]]),
                #     "police_verification": ".".join([str(uuid.uuid4()), files["police_verification"].content_type.split('/')[1]])
                # }

                params = {
                    mapping[doc_type]: ".".join([str(uuid.uuid4()), files[mapping[doc_type]].content_type.split('/')[1]])
                }

                # # only valid file names
                # valid_fields = ["primary_id", "secondary_id", "police_verification"]

                # for key in files.keys():
                #     if key not in valid_fields:
                #         return ResponseUtil(False, 403).message_response("Invalid field {}".format(key))

                # for key in ["secondary_id", "police_verification"]:
                #     if key not in files.keys():
                #         return ResponseUtil(False, 403).message_response("{} required".format(key))

                # for key in files.keys():
                #     if not self.allowed_file(files[key].filename):
                #         return ResponseUtil(False, 403).message_response("{} invalid filetype".format(key))

                # save documents files

                files[mapping[doc_type]].save(os.path.join(
                    app.config['UPLOAD_FOLDER'], doc_type, params[mapping[doc_type]]))

                # files["primary_id"].save(os.path.join(
                #     app.config['UPLOAD_FOLDER'], 'primary', filenames['primary']))
                # files["secondary_id"].save(os.path.join(
                #     app.config['UPLOAD_FOLDER'], 'secondary', filenames['secondary']))
                # files["police_verification"].save(os.path.join(
                #     app.config['UPLOAD_FOLDER'], 'police_verification', filenames['police_verification']))

                print("files: ", params)

                response = VerificationDocs().add(user_id, params)

                return response

            else:
                return ResponseUtil(False, 403).message_response("Unrecognized DOC Type")

        except Exception as e:
            print(e)
            app.logger.error(
                "Exception while buddy verification: {}".format(e))
            return ResponseUtil(False, 403).message_response("Invalid request. Exception: {}".format(e))


class BuddyBank(Resource):

    @allowed_api_roles([C.APP_ROLES.get('BUDDY'), C.APP_ROLES.get('ADMIN')])
    def get(self, buddy_id=None, **kwargs):
        try:
            if buddy_id:
                user_id = buddy_id
            else:
                user_id = kwargs["payload"]["user_id"]
            return BankDetails().get_one(user_id)
        except Exception as e:
            return ResponseUtil(False, 403).message_response("Invalid request. Exception: {}".format(e))

    @allowed_api_roles([C.APP_ROLES.get('BUDDY')])
    def post(self, **kwargs):
        try:
            json_data = request.get_json(force=True)
            user_id = kwargs["payload"]["user_id"]
            return BankDetails().add_details(user_id, json_data)

        except Exception as e:
            return ResponseUtil(False, 403).message_response("Invalid request. Exception: {}".format(e))


class UserFeed(Resource):

    @allowed_api_roles([C.APP_ROLES.get('STUDENT')])
    def get(self, **kwargs):
        if "login_as" in kwargs['payload'] and kwargs['payload']["login_as"] == "STUDENT":
            count = request.args.to_dict().get("count")

            if count and str(count).isdigit():
                count = int(count)
            else:
                count = 0

            params = {"visible": True}

            return TimelineFeed().get_all(params, count)
        else:
            return ResponseUtil(False, 403).message_response("Could not identify current login role")

    @allowed_api_roles([C.APP_ROLES.get('STUDENT')])
    def post(self, **kwargs):
        try:
            json_data = request.get_json(force=True)
            json_data["user_id"] = kwargs["payload"]["user_id"]

            return TimelineFeed().add_one(json_data)

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")

    @allowed_api_roles([C.APP_ROLES.get('STUDENT')])
    def delete(self, **kwargs):
        try:
            json_data = request.get_json(force=True)
            json_data["user_id"] = kwargs["payload"]["user_id"]

            return TimelineFeed()._delete(json_data)

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")


class Bookmark(Resource):

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def get(self, role=None, **kwargs):
        try:

            self_id = kwargs['payload']['user_id']

            return Student().get_bookmarked_profiles(self_id, service_model=Service, package_model=Package)

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Incomplete request")

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def post(self, role=None, **kwargs):
        try:
            json_data = request.get_json(force=True)

            if all(x in json_data for x in ["user_id", "role"]):
                self_id = kwargs['payload']['user_id']

                if self_id != json_data["user_id"]:
                    return Student().bookmark_profile(self_id, json_data)
                else:
                    return ResponseUtil(False, 403).message_response("You can not Bookmark your own profile")

            else:
                return ResponseUtil(False, 403).message_response("Incomplete/Invalid json request")
        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def delete(self, role=None, **kwargs):
        try:
            json_data = request.get_json(force=True)

            if all(x in json_data for x in ["user_id", "role"]):
                self_id = kwargs['payload']['user_id']

                if self_id != json_data["user_id"]:
                    return Student().bookmark_profile_delete(self_id, json_data)
                else:
                    return ResponseUtil(False, 403).message_response("You can not Bookmark your own profile")

            else:
                return ResponseUtil(False, 403).message_response("Incomplete/Invalid json request")
        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")


class UserBuddy(Resource):

    @allowed_api_roles([C.APP_ROLES.get('STUDENT')])
    def post(self, **kwargs):
        try:
            json_data = request.get_json(force=True)
            # print('json_data = = =', json_data)
            # print('kwargs = = =', kwargs)

            if "buddy_id" in json_data:
                return Student().select_buddy(kwargs['payload']['user_id'], json_data["buddy_id"], 
                                                ServiceTask, 
                                                Package,  
                                                json_data["amount_to_kh"], json_data["amount_to_kh_inr"], json_data["payment_id"])
            else:
                return ResponseUtil(False, 403).message_response("Buddy ID required")

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")

    @allowed_api_roles([C.APP_ROLES.get('STUDENT')])
    def delete(self, **kwargs):
        json_data = request.get_json(force=True)
        response = Student().deselect_buddy(kwargs['payload']['user_id'], json_data)
        
        if response["status"]:
            try:
                student_obj = User.objects.get(_id=kwargs['payload']['user_id'])
                buddy_id = Student.objects.get(user_id=kwargs['payload']['user_id']).hired_buddy
                buddy_obj = User.objects.get(_id=buddy_id)

                # send email to buddy
                try:
                    email_obj = Email_(deepcopy(C.EMAIL_TEMPLATES['buddy_reported']))
                    
                    format_data = {
                        "MENTOR_NAME": buddy_obj.first_name,
                        "STUDENT_NAME": ' '.join([student_obj.first_name, student_obj.last_name])
                    }

                    email_params = {
                        "to_email": buddy_obj.email
                    }

                    email_obj.template['message'] = email_obj.template['message'].format(**format_data)

                    email_response = email_obj.send_mail(email_params)
                    del email_obj
                    app.logger.error("reported issue email response {}".format(str(email_response)))

                except Exception as e:
                    app.logger.error('failed to send reported issue email to buddy error {}'.format(str(e)))

                #Send an email to admin
                try:
                    buddy_name = " ".join([buddy_obj.first_name, buddy_obj.last_name])

                    email_obj = Email_(deepcopy(C.EMAIL_TEMPLATES["report_issue_admin"]))
                    email_obj.template["message"] = email_obj.template["message"].format(
                        " ".join([student_obj.first_name, student_obj.last_name]),
                        " ".join([buddy_obj.first_name, buddy_obj.last_name]),
                        json_data["issue"])
                    
                    email_params = {
                        "to_email": "harshad.blues@gmail.com"
                    }
                    email_response = email_obj.send_mail(email_params)
                    del email_obj
                except Exception as e:
                    app.logger.error('failed to send reported issue email to admin error {}'.format(str(e)))
            except Exception as e:
                app.logger.error('failed to send reported issue email error {}'.format(str(e)))


        return response


class Messages(Resource):

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def get(self, receiver_id=None, count=None, **kwargs):

        if not receiver_id:
            return ResponseUtil(False, 403).message_response("receiver_id required")
        else:

            url_params = request.args.to_dict()

            params = {
                "sender_id": kwargs['payload']['user_id'],
                "receiver_id": receiver_id,
                "offset": url_params.get("offset")
            }

            return Message().get_all(params)

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def post(self, receiver_id=None, **kwargs):
        try:
            json_data = request.get_json(force=True)
            json_data['sender_id'] = kwargs['payload']['user_id']

            if not "receiver_id" in json_data:
                return ResponseUtil(False, 403).message_response("receiver_id required")
            else:
                return Message().add_one(json_data)

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def put(self, **kwargs):
        try:
            json_data = request.get_json(force=True)
            json_data['receiver_id'] = kwargs['payload']['user_id']

            if Block()._update(json_data):
                return ResponseUtil(True, 200).message_response("Changed the blocked status")

            else:
                return ResponseUtil(False, 200).message_response("Internal server Error")

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")


class ChatList(Resource):

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def get(self, **kwargs):
        params = {
            "user_id": kwargs['payload']['user_id']
        }
        return Message().get_chat_list(params)

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def delete(self, **kwargs):
        try:
            json_data = request.get_json(force=True)
            json_data["user_id"] = kwargs['payload']['user_id']
            return Message().delete_messages(json_data)
        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")


class DeleteJunk(Resource):

    def get(self):
        buddies = Buddy.objects.all()

        deleted = {
            "buddies": 0,
            "students": 0,
            "feeds": 0,
            "reviews": 0,
            "lastseen": 0,
            "messages": 0,
            "reported_issues": 0,
            "docs": 0
        }

        for buddy in buddies:
            if not User.objects(_id=buddy.user_id):
                Buddy.objects(user_id=buddy.user_id).delete()
                deleted["buddies"] += 1

        students = Student.objects.all()

        for student in students:
            if not User.objects(_id=student.user_id):
                Student.objects(user_id=student.user_id).delete()
                deleted["students"] += 1

        feeds = TimelineFeed.objects.all()

        for feed in feeds:
            if not User.objects(_id=feed.user_id):
                TimelineFeed.objects(user_id=feed.user_id).delete()
                deleted["feeds"] += 1

        reviews = Review.objects.all()

        for review in reviews:
            if not User.objects(_id=feed.user_id):
                Review.objects(user_id=review.user_id).delete()
                deleted["reviews"] += 1

        lastseens = LastSeen.objects.all()

        for lastseen in lastseens:
            if not User.objects(_id=lastseen.self_id):
                LastSeen.objects(self_id=lastseen.self_id).delete()
                deleted["lastseen"] += 1

            if not User.objects(_id=lastseen.user_id):
                LastSeen.objects(user_id=lastseen.user_id).delete()
                deleted["lastseen"] += 1

        messages = Message.objects.all()

        for message in messages:
            if not User.objects(_id=message.sender_id):
                Message.objects(sender_id=message.sender_id).delete()
                deleted["messages"] += 1

            if not User.objects(_id=message.receiver_id):
                Message.objects(receiver_id=message.receiver_id).delete()
                deleted["messages"] += 1

        verificationdocs = VerificationDocs.objects.all()

        for docs in verificationdocs:
            if not User.objects(_id=docs.user_id):
                VerificationDocs.objects(user_id=docs.user_id).delete()
                deleted["docs"] += 1

        reported_issues = ReportIssue.objects.all()

        for issue in reported_issues:
            if not User.objects(_id=issue.student_id):
                ReportIssue.objects(student_id=issue.student_id).delete()
                deleted['reported_issues'] += 1
            
            if not User.objects(_id=issue.buddy_id):
                ReportIssue.objects(buddy_id=issue.buddy_id).delete()
                deleted['reported_issues'] += 1

        return ResponseUtil(True, 200).data_response(deleted)


class AvailableDates(Resource):

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def get(self, student_id=None, **kwargs):
        if kwargs["payload"]["login_as"].upper() == "BUDDY":
            student_obj = json.loads(Student.objects(user_id=student_id)
                                     .only(*C.STUDENT_INFO_FOR_STUDENT).to_json())[0]

        else:
            student_obj = json.loads(Student.objects(user_id=kwargs["payload"]["user_id"])
                                     .only(*C.STUDENT_INFO_FOR_STUDENT).to_json())[0]

        try:
            availability_dates = {}
            students_dates = student_obj.pop("student_availability")
            buddy_dates = student_obj.pop("buddy_availability")

            for date in buddy_dates:
                availability_dates[date] = ["buddy"]

            for date in students_dates:
                if date in availability_dates:
                    availability_dates[date].append('student')
                else:
                    availability_dates[date] = ["student"]

            student_obj["availability_dates"] = availability_dates
            return ResponseUtil(True, 200).data_response({"availability_dates": availability_dates})

        except Exception as e:
            app.logger.error(
                "exception while getting travelling info for student: {}".format(e))
            return ResponseUtil(False, 403).message_response("User did not updated travelling details yet")

    @allowed_api_roles([C.APP_ROLES.get('STUDENT'), C.APP_ROLES.get('BUDDY')])
    def post(self, **kwargs):
        try:
            json_data = request.get_json(force=True)

            try:
                if kwargs["payload"]["login_as"].upper() == "BUDDY":
                    if "student_id" in json_data:
                        student_id = json_data["student_id"]
                        if json_data["action"] == 'add':
                            Student.objects(user_id=student_id).update(
                                add_to_set__buddy_availability=json_data["date"])
                        else:
                            Student.objects(user_id=student_id).update(
                                pull__buddy_availability=json_data["date"])

                    else:
                        return ResponseUtil(False, 403).message_response("User id required")
                else:
                    user_id = kwargs['payload']['user_id']
                    if json_data["action"] == 'add':
                        Student.objects(user_id=user_id).update(
                            add_to_set__student_availability=json_data["date"])
                    else:
                        Student.objects(user_id=user_id).update(
                            pull__student_availability=json_data["date"])

                return ResponseUtil(True, 200).message_response("Added Successfully")

            except Exception as e:
                app.logger.error(e)
                return ResponseUtil(False, 403).message_response(
                    "Could not update user info".format(e))

        except Exception as e:
            print(e)
            return ResponseUtil(False, 403).message_response("Invalid json request")


class AddDefaults(Resource):

    def get(self):
        added = {
            "students": 0,
            "buddies": 0
        }

        for user in User.objects(roles="kh_student"):
            if not Student.objects(user_id=user._id):
                Student().add_default(str(user._id))
                added["students"] += 1

        for user in User.objects(roles="kh_buddy"):
            if not Buddy.objects(user_id=user._id):
                Buddy().add_default(str(user._id))
                added["buddies"] += 1

        return ResponseUtil(True, 200).data_response(added)


class Payment(Resource):

    @allowed_api_roles([C.APP_ROLES.get('STUDENT')])
    def get(self, action=None, **kwargs):
        # Call the payment api for the link

        try:
            if not action:
                return ResponseUtil(False, 403).message_response("Action required")

            else:
                args = request.args.to_dict()
                student_id = kwargs["payload"]["user_id"]
                args["student_id"] = student_id
                if action == 'link':
                    # if args["is_transaction_complete"] == 'true':
                    #     Student.objects(user_id=student_id).update_one(hired_buddy=args["buddy_id"])
                    #     ServiceTask().add_default(student_id, args["buddy_id"], args["package_id"])
                    #     return ResponseUtil(True, 200).message_response("Mentor Hired Successfully")
                    # else:
                    response = Transaction().get_link(args, Package)
                    return response

                elif action == 'details':
                    transaction_obj = json.loads(
                        Transaction.objects(student_id=student_id).to_json())
                    if not transaction_obj:
                        return ResponseUtil(False, 403).message_response("No payment record found")
                    else:
                        return ResponseUtil(True, 200).json_data_response({'details': transaction_obj[0]})

        except Exception as e:
            app.logger.error(
                "Exception while generating a payment link {}".format(e))
            return ResponseUtil(False, 403).message_response("Exception {}".format(e))

    @allowed_api_roles([C.APP_ROLES.get('ADMIN')])
    def post(self, **kwargs):
        try:
            json_data = request.get_json(force=True)

            print(json_data)

            Transaction.objects(_id=json_data["transaction_id"]).update(**{
                "amount_paid_to_buddy": json_data["amount_paid_to_buddy"],
                "remarks": json_data["remarks"]
            })

            return ResponseUtil(True, 200).message_response("Database Updated")

        except Exception as e:
            return ResponseUtil(False, 403).message_response("Exception {}".format(e))


@app.route('/payment/callback', methods=["GET"])
def PaymentCallback(**kwargs):
    
    try:
        args = request.args.to_dict()
        payment_id = args.get('payment_id')
        payment_request_id = args.get('payment_request_id')
        response_status = PaymentInstamojo().get_status(payment_request_id)
        if response_status['success']:
            failure_status = response_status['payment_request']['payments'][0]['failure']
            if not failure_status:
                transaction_obj = Transaction.objects.get(
                    payment_request_id=payment_request_id)
                student_id = transaction_obj.student_id
                buddy_id = transaction_obj.buddy_id
                package_id = transaction_obj.package_id
                
                if all(i for i in [student_id, buddy_id, package_id]):
                    serv_task_response = ServiceTask().add_default(student_id, buddy_id, package_id)
                    if serv_task_response['status']:
                        updated = Student.objects(user_id=student_id).update_one(hired_buddy=buddy_id)
                        if not updated:
                            app.logger.error("hired_buddy {} not updated for student {}".format(buddy_id, student_id))
                            raise Exception

                    else:
                        app.logger.error("Failed to add default services")
                        raise Exception

                else:
                    app.logger.error("student_id:{}, buddy_id:{}, package_id:{}".format(student_id, buddy_id, package_id))
                    app.logger.error("Exception while updating status for payment_request_id {} because of error {}".format(
                        payment_request_id, "ids not found"))
                    raise Exception

                # push_notification_params = {
                #     'student_id': student_id,
                #     'buddy_id': buddy_id,
                #     'title': "You have been Hired!",
                #     'message': "Congratulations! You have been booked by {}"
                # }
                
                try:
                    try:
                        student_obj = User.objects.get(_id=student_id)
                        student_full_name = ' '.join([student_obj.first_name, student_obj.last_name])
                    except Exception as e:
                        student_full_name = ''

                    push_notification_params = {
                        'title': "You have a new client!",
                        'message': "Congratulations! You have a new client {}".format(student_full_name)
                        # 'message': json.dumps({
                        #     'type': 'MENTOR_HIRED',
                        #     'message': "Congratulations! You have a new client {}".format(student_full_name)
                        # })
                    }

                    registration_id = User.objects.get(
                        _id=buddy_id
                    ).fcm_token

                    PushNotification(
                        registration_id
                    ).send_display_message(push_notification_params)
                except Exception as e:
                    app.logger.error("failed to send MENTOR_HIRED push notification")


                Transaction.objects(payment_request_id=payment_request_id).update(
                    status_to_kh=response_status['payment_request']['status'],
                    payment_id=payment_id,
                    completed=True,
                    payment_to_edapt_time=datetime.utcnow())

                # transaction successful
                try:
                    buddy_obj = User.objects.get(_id=buddy_id)
                    buddy_name = " ".join([buddy_obj.first_name, buddy_obj.last_name])
                    student_obj = User.objects.get(_id=student_id)
                    pkg_obj = Package.objects.get(_id=package_id)
                    
                    # send email notification to buddy
                    try:
                        format_data = {
                            "MENTOR_NAME" : 'Mentor',
                            "STUDENT_NAME" : 'Unknown Student',
                            "PACKAGE_NAME" : 'Unknown Package'
                        }
                        
                        try:
                            format_data["MENTOR_NAME"] = buddy_obj.first_name
                            format_data["STUDENT_NAME"] = student_obj.first_name
                            format_data["PACKAGE_NAME"] = pkg_obj.name
                        except Exception as e:
                            pass

                        email_obj = Email_(deepcopy(C.EMAIL_TEMPLATES['hired_buddy']))

                        email_obj.template['message'] = email_obj.template['message'].format(**format_data)
                        
                        email_params = {
                            "to_email": buddy_obj.email
                        }
                        email_response = email_obj.send_mail(email_params)
                        del email_obj
                        app.logger.error("hired buddy email response {}".format(str(email_response)))
                    except Exception as e:
                        app.logger.error("failed to send hired buddy email error {}".format(str(e)))

                except Exception as e:
                    buddy_name = "a mentor"
                
                return render_template('redirect_payment.html', data={
                    "payment_status": "Success",
                    "message": "Congrats! You have successfully hired {}".format(buddy_name),
                })

            else:
                # transaction failed

                return render_template('redirect_payment.html', data={
                    "payment_status": "Failed",
                    "message": "Uh ho! Your transaction could not be processed. Please try again"
                })

        else:
            app.logger.error("Api call to Instamojo failed")
            raise Exception

    except Exception as e:
        print("error", e)
        app.logger.error("Exception while updating status for payment_request_id {} because of error {}".format(
            payment_request_id, e))

        # server error, contact edapt team

        return render_template('redirect_payment.html', data={
            "payment_status": "Error",
            "message": "Oops! something went wrong. Please contact contact@khedapt.com to resolve this payment issue."
        })



# class ImageUrl(Resource):
#     def get(self):
#         user_objs = User.objects()

#         matched = len(user_objs)
#         updated = 0

#         for usr in user_objs:
#             if not usr.image.startswith("/"):
#                 usr.image = "/" + usr.image
#                 usr.save()
#                 updated += 1

#         return ResponseUtil(True, 200).data_response({"matched": matched, "updated": updated})

class does_exists(Resource):
    def post(self):
        return User().does_exists(request.json['username'])

class report_user(Resource):
    def post(self):
        username = request.json['username']
        name = request.json['name']

        email_obj = Email_(deepcopy(C.EMAIL_TEMPLATES["report_user"]))
        email_obj.template['message'] = email_obj.template[
                'message'].format(
                    username=username, name=name)
        email_params = {"to_email": C.feedback_report_emails[0]}
        email_response = email_obj.send_mail(email_params)

        email_params = {"to_email": C.feedback_report_emails[1]}
        email_response = email_obj.send_mail(email_params)


        email_params = {"to_email": C.feedback_report_emails[2]}
        email_response = email_obj.send_mail(email_params)

        del email_obj

        return ResponseUtil(True, 200).json_data_response({
            "username":username,
            "name":name
        })