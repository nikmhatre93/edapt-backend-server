# custom response template
from flask import Response
import json
import random
import string
from bson import json_util
from app import app
import sendgrid
from sendgrid.helpers.mail import *


def get_random_code():
    return ''.join(random.choice(string.ascii_uppercase) for _ in range(4)) + ''.join(
        random.choice(string.digits) for _ in range(4))


def generate_otp():
    return random.randint(1000, 9999)


class ResponseUtil:
    status = ""

    def __init__(self, status, code):
        self.status = status
        self.code = code

    def json_message_response(self, message):
        return Response(json.dumps({
            "status": self.status,
            "code": self.code,
            "message": message
        }), mimetype="application/json")

    def json_data_response(self, data):
        return Response(json.dumps({
            "status": self.status,
            "code": self.code,
            "data": data
        }, default=json_util.default), mimetype="application/json")

    def message_response(self, message):
        return {
            "status": self.status,
            "code": self.code,
            "message": message
        }

    def data_response(self, data):
        return {
            "status": self.status,
            "code": self.code,
            "data": data
        }

    def exception_response(self, exception):
        exception_message_body = json.loads(exception.body)

        if "errors" in exception_message_body:
            error_message = json.dumps(exception_message_body["errors"])
        elif "error" in exception_message_body:
            error_message = json.dumps(exception_message_body["error"])
        else:
            error_message = "Something went wrong"
        return self.json_message_response(json.loads(error_message))


def get_sorted(list_of_dict, var, reverse_sort=False):

    try:
        date_fields = ["created", "updated", "payment_to_edapt_time"]

        if var in date_fields:
            return sorted(list_of_dict, key=lambda k: k[var]["$date"], reverse=reverse_sort)

        else:
            return sorted(list_of_dict, key=lambda k: k[var])

    except Exception as e:
        app.logger.error("Exception while sorting results {}".format(e))
        return list_of_dict
