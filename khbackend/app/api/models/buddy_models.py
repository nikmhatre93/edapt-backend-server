from mongoengine import Document, fields
from ..controllers.custom_utils import ResponseUtil, get_random_code, get_sorted
from libs.push_notification import *
import datetime
from libs import tokenize
import conf.constants as C
from bson import ObjectId
import json
import urllib.parse
from app import app
import uuid
import os
import base64
from PIL import Image
from io import BytesIO
import requests
from libs.payments import *


class UserBuddy(Document):
    _id = fields.ObjectIdField()
    username = fields.StringField(required=True)
    password = fields.StringField(required=True)
    first_name = fields.StringField(required=True)
    last_name = fields.StringField(required=True)
    sex = fields.StringField(default="")
    about = fields.StringField(default="")
    country_code = fields.StringField(required=True, default="")
    contact = fields.StringField(required=True)
    email = fields.StringField(default="")
    sms_otp = fields.IntField(default=None)
    otp_valid_till = fields.DateTimeField()
    home_address = fields.StringField(default="")
    city = fields.StringField(default="")
    origin = fields.StringField(default="")
    country = fields.StringField(default="")
    university = fields.StringField(default="")
    date_of_birth = fields.DynamicField(default="")
    mother_tongue = fields.StringField(default="")
    languages = fields.ListField(default=[])
    image = fields.StringField(
        default="/static/image/default_user.png")
    education = fields.StringField(default="")
    employment = fields.StringField(default="")
    token = fields.StringField(required=True)
    login_type = fields.StringField(required=True)
    is_buddy = fields.BooleanField(default=False)
    is_active = fields.BooleanField(default=False)
    is_deleted = fields.BooleanField(default=False)
    is_contact_verified = fields.BooleanField(default=False)
    roles = fields.ListField(fields.StringField())
    created = fields.DateTimeField()
    updated = fields.DateTimeField()
    home_town = fields.StringField(default="")
    current_city = fields.StringField(default="")
    current_country = fields.StringField(default="")
    change_password_token = fields.StringField(default="")
    fcm_token = fields.StringField(default="")

    meta = {"collection": "user_buddy"}
