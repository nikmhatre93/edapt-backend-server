from mongoengine import Document, fields
from ..controllers.custom_utils import ResponseUtil, get_random_code
from libs.push_notification import *
import datetime
from libs import tokenize
import conf.constants as C
from bson import ObjectId
import json
import urllib.parse
from app import app
from .user_models import User, Buddy
import bson


class feedbacks(Document):
    name = fields.StringField(default="")
    email = fields.StringField(default="")
    msg = fields.StringField(default="")
    created = fields.DateTimeField(default=datetime.datetime.utcnow())

    def addnew(self, data):
        try:
            obj = self.__class__.objects.create(**data).save()
            return ResponseUtil(True, 200).data_response(data)
        except Exception as e:
            return ResponseUtil(False, 500).message_response("Something went wrong.")
    

class Service(Document):
    _id = fields.ObjectIdField()
    user_id = fields.StringField()

    name = fields.StringField()
    details = fields.StringField(default="")

    created = fields.DateTimeField()
    updated = fields.DateTimeField()

    meta = {"collection": "service"}

    def get_one(self, service_id=None):
        if service_id:
            try:
                service_obj = self.__class__.objects.get(_id=service_id)

                response_data = {
                    "name": service_obj.name,
                    "details": service_obj.details
                }

                return ResponseUtil(True, 200).data_response(response_data)

            except Exception as e:
                print(e)
                return ResponseUtil(False,
                                    404).message_response("Invalid Service ID")
        else:
            return ResponseUtil(False,
                                403).message_response("Service ID missing")

    def get_all(self, user_id=None):
        if not self.__class__.objects(user_id=user_id):
            return ResponseUtil(False,
                                404).message_response("No services found")
        else:
            services_json = self.__class__.objects(user_id=user_id).only(
                "_id", "name", "details").to_json()

            return ResponseUtil(True, 200).json_data_response(
                json.loads(services_json))

    def get_dict(self):
        if not self.__class__.objects():
            return {}
        else:
            services = self.__class__.objects().only("_id", "name", "details")

            service_dict = {}

            for service in services:
                service_dict[str(service["_id"])] = {
                    "_id": {
                        "$oid": str(service["_id"])
                    },
                    "name": service["name"],
                    "details": service["details"]
                }

            return service_dict

    def add_one(self, params):
        try:
            if not self.__class__.objects(
                    name=params["name"], user_id=params["user_id"]):
                created = self.__class__.objects.create(**params)

                return ResponseUtil(
                    True, 200).message_response("Service added successfully")

            else:
                return ResponseUtil(False, 403).message_response(
                    "Service with same name already exists")

        except Exception as e:
            print(e)
            return ResponseUtil(
                False,
                403).message_response("Failed to add a service, Exception: " +
                                      str(e))

    def _update(self, params):
        try:
            service_id = params.pop("service_id")
            updated = self.__class__.objects(_id=service_id).update_one(
                **params)

            if updated:
                service_name = self.__class__.objects.get(_id=service_id).name
                return ResponseUtil(True, 200).message_response(
                    "{} is now complete, Great stuff!".format(service_name))
            else:
                return ResponseUtil(
                    False, 403).message_response("Service id not found")

        except Exception as e:
            return ResponseUtil(False, 403).message_response(
                "Exception: {}".format(e))

    def delete_one(self, params):
        pass

    def add_default(self):
        added = 0
        exists = 0
        for service_name in C.SERVICES:
            if not self.__class__.objects(
                    name=service_name, user_id="default"):
                self.__class__.objects.create(
                    name=service_name,
                    user_id="default",
                    created=datetime.datetime.utcnow(),
                    updated=datetime.datetime.utcnow())
                added += 1
            else:
                exists += 1

        response_data = {"services_added": added, "already_exists": exists}
        return ResponseUtil(True, 200).data_response(response_data)

    def get_default(self):
        services = json.loads(
            self.__class__.objects(user_id="default").only(
                "_id", "name", "details", "created").to_json())
        if services:
            return ResponseUtil(True, 200).data_response({
                "services": services,
                "count": len(services)
            })
        else:
            return ResponseUtil(
                False, 404).message_response("No default Services found")

    def get_all_services(self):
        data = []
        services_json = self.__class__.objects().only("_id", "name")
        for i in services_json:
            dct = {
                "value": str(bson.objectid.ObjectId(i._id)),
                "label": i.name
            }
            data.append(dct)
        services_json = json.dumps(data)
        return ResponseUtil(True, 200).json_data_response(
            json.loads(services_json))


class Package(Document):
    _id = fields.ObjectIdField()
    details = fields.StringField(default="")
    name = fields.StringField(default="")
    buddy_id = fields.ObjectIdField()
    amount = fields.IntField(default="")
    services = fields.ListField(fields.StringField())
    created = fields.DateTimeField()
    updated = fields.DateTimeField()

    meta = {"collection": "package"}

    def get_one(self, pkg_id=None):
        if pkg_id:
            try:
                pkg_obj = self.__class__.objects.get(_id=pkg_id)

                services = Service().get_dict()

                if services and pkg_obj.services:
                    package_services = []

                    for service_id in pkg_obj.services:
                        package_services.append(services[service_id])

                    response_data = {
                        "name": pkg_obj.name,
                        "details": pkg_obj.details,
                        "amount": pkg_obj.amount,
                        "services": package_services
                    }

                return ResponseUtil(True, 200).data_response(response_data)

            except Exception as e:
                app.logger.error(
                    "Exception while getting package info for package id: {}, Exception: {}"
                    .format(pkg_id, e))
                return ResponseUtil(False,
                                    404).message_response("Invalid Package ID")
        else:
            return ResponseUtil(False,
                                403).message_response("Package ID missing")

    def get_all(self):

        if not self.__class__.objects:
            return ResponseUtil(False,
                                404).message_response("No packages found")
        else:
            packages = json.loads(self.__class__.objects().only(
                "_id", "name", "details", "amount", "services", "created",
                "updated").order_by('amount').to_json())

            if packages:

                services = Service().get_dict()

                for package in packages:
                    if "services" in package and services:
                        pkg_service_ids = package.pop("services")

                        package_services = []

                        for service_id in pkg_service_ids:
                            package_services.append(services[service_id])

                        package["services"] = package_services

                return ResponseUtil(True, 200).data_response({
                    "packages":
                    packages
                })
            else:
                return ResponseUtil(False,
                                    404).message_response("No packages found")

    def get_dict(self):
        if not self.__class__.objects():
            return {}
        else:
            packages = self.__class__.objects().only("_id", "name", "details",
                                                     "amount", "services")
            package_dict = {}

            services = Service().get_dict()

            for package in packages:
                # print()
                package_dict[str(package["_id"])] = {
                    "_id": {
                        "$oid": str(package["_id"])
                    },
                    "name": package["name"],
                    "details": package["details"],
                    "amount": package["amount"],
                    "services":
                    [services[i]["name"] for i in package["services"] if services[i]]
                }

            return package_dict

    def add_one(self, params=None):

        if self.__class__.objects(name=params["name"]):
            return ResponseUtil(
                False,
                403).message_response("Package with same name already exists")
        else:
            try:

                valid_fields = ["name", "details", "amount", "services"]

                if all(x in valid_fields
                       for x in params.keys()) and all(x in params.keys()
                                                       for x in valid_fields):

                    self.__class__.objects.create(**params)

                else:

                    return ResponseUtil(
                        False,
                        403).message_response("Invalid/Incomplete fields")

                # print(package_obj)
                return ResponseUtil(
                    True, 200).message_response("Package added successfully")
            except Exception as e:
                return ResponseUtil(False, 403).message_response(
                    "Failed to add a package. Exception: " + str(e))

    def delete_one(self, params=None):
        pass

    def add_new(self, params=None):
        try:
            data = self.__class__.objects.create(**params)
            b = Buddy.objects.get(user_id=params['buddy_id'])
            for i in b:
                if i == 'package_id':
                    b[i].append(str(bson.objectid.ObjectId(data.auto_id_0)))
                    b['is_package_selected'] = True
                    b.save()
            return ResponseUtil(True, 200).data_response({
                'Package_id':
                str(data.auto_id_0)
            })
        except Exception as e:
            return ResponseUtil(
                False,
                403).message_response("Failed to add a package. Exception: " +
                                      str(e))

    def update_package(self,params):
        try:
            d = self.__class__.objects(_id=params['id']).update(**params['data'])
            return ResponseUtil(True, 200).data_response({})
        except Exception as e:
            return ResponseUtil(False, 403).message_response("Failed to update a package. Exception: " + str(e))
    
    def get_buddy_packages(self, buddy_id):
        try:
            pkgs =  self.__class__.objects(buddy_id=buddy_id)
            services = Service().get_dict()
            data = []
            for i in pkgs:
                print(i._id)
                obj = {
                    "_id":str(bson.objectid.ObjectId(i._id)),
                    "name":i.name,
                    "details":i.details,
                    "amount":i.amount,
                    "services":[ {"value":str(bson.objectid.ObjectId(services[j]['_id']['$oid'])), "label":services[j]['name']} for j in i.services]
                }
                data.append(obj)
                data = json.dumps(data)
                # print('data', data)
                return ResponseUtil(True, 200).data_response(json.loads(data))
                # return None
        except Exception as e:
            return ResponseUtil(False, 403).message_response("Failed to update a package. Exception: " + str(e))

class ServiceTask(Document):
    _id = fields.ObjectIdField()
    service_id = fields.StringField()

    buddy_id = fields.StringField()
    student_id = fields.StringField()
    # amount = fields.StringField(default="")

    checked_by_student = fields.BooleanField(default=False)
    checked_by_buddy = fields.BooleanField(default=False)

    completed = fields.BooleanField(default=False)

    # last_date = fields.DateTimeField()
    completed_on = fields.DateTimeField()
    created = fields.DateTimeField()

    meta = {"collection": "service_task"}

    def add_default(self, student_id, buddy_id, pkg_id):
        try:
            services = Package.objects.get(_id=pkg_id).services
            utc_now = datetime.datetime.utcnow()
            # amount = Package.objects.get(_id=pkg_id).amount
            # print(amount, type(amount))

            for service_id in services:
                if not self.__class__.objects(
                        service_id=service_id,
                        student_id=student_id,
                        buddy_id=buddy_id):
                    self.__class__.objects.create(
                        service_id=service_id,
                        buddy_id=buddy_id,
                        student_id=student_id,
                        checked_by_student=False,
                        checked_by_buddy=False,
                        completed=False,
                        completed_on=None,
                        # amount=str(amount),
                        created=utc_now)

            return {"status": True}

        except Exception as e:
            app.logger.error("Exception while adding default service tasks for student: {}, buddy: {}, Exception: {}"\
                .format(student_id, buddy_id, e))

            return {"status": False}

    # def get_amount(self, student_id):
    #     amount = json.loads(self.__class__.objects(student_id=student_id).to_json())[0]["amount"]
    #     return amount

    def get_all(self, query_params):
        # query_params fields = (student_id, buddy_id) / (task_id)

        tasks = json.loads(
            self.__class__.objects(**query_params).only(
                "_id", "service_id", "checked_by_student", "checked_by_buddy",
                "completed").to_json())

        if tasks:
            task_list_response = []

            tasks_completed = 0
            tasks_remaining = 0

            for task in tasks:

                try:
                    service_obj = Service.objects.get(_id=task["service_id"])

                    task_data = {
                        "_id": task["_id"],
                        "name": service_obj.name,
                        "details": service_obj.details,
                        "checked_by_student": task["checked_by_student"],
                        "checked_by_buddy": task["checked_by_buddy"],
                        "completed": task["completed"],
                        # "last_date": task["last_date"],
                        # "completed_on": task["completed_on"]
                    }

                    if task_data["completed"]:
                        tasks_completed += 1
                    else:
                        tasks_remaining += 1

                    task_list_response.append(task_data)

                except Exception as e:
                    print(e)
            if task_list_response:
                return ResponseUtil(True, 200).data_response({
                    "tasks":
                    task_list_response,
                    "tasks_total":
                    len(task_list_response),
                    "tasks_completed":
                    tasks_completed,
                    "tasks_remaining":
                    tasks_remaining,
                    "percentage":
                    int((tasks_completed / len(task_list_response)) * 100)
                })
            else:
                return ResponseUtil(
                    False, 403).message_response("Failed to fetch user tasks")

        else:
            return ResponseUtil(False, 404).message_response("No tasks found")

    def _update(self, params):
        task_id = params.pop("task_id")
        task_obj = self.__class__.objects(_id=task_id)

        if task_obj:
            if task_obj.filter(completed=False):
                task_obj.update(**params)

                if task_obj.filter(
                        checked_by_buddy=True, checked_by_student=True):
                    task_obj.update_one(
                        completed=True,
                        completed_on=datetime.datetime.utcnow())

                    # send push notification
                    try:
                        send_notification = True

                        # task name , marked by user- name
                        service_id = task_obj[0].service_id
                        service_name = Service.objects.get(_id=service_id).name

                        if "checked_by_student" in params and params[
                                "checked_by_student"]:
                            user_id = task_obj[0].student_id
                            registration_id = User.objects.get(
                                _id=task_obj[0].buddy_id).fcm_token
                        elif "checked_by_buddy" in params and params[
                                "checked_by_buddy"]:
                            user_id = task_obj[0].buddy_id
                            registration_id = User.objects.get(
                                _id=task_obj[0].student_id).fcm_token
                        else:
                            send_notification = False

                        if send_notification:
                            user_name = User.objects.get(
                                _id=user_id).first_name
                            params = {
                                "title":
                                "Service Task completed!",
                                # 'message': json.dumps({
                                #     'type': 'SERVICE_TASK_UPDATE',
                                #     "message": "{} marked as completed by {}".format(service_name,user_name)
                                # })
                                "message":
                                "{} marked as completed by {}".format(
                                    service_name, user_name)
                            }

                            display_response = PushNotification(
                                registration_id).send_display_message(params)

                            if display_response["success"] != 1:
                                app.logger.error(
                                    "Failed to send push notification")
                            else:
                                print("\n\npushnotification sent\n\n")

                    except Exception as e:
                        app.logger.error(
                            "Exception while sending push notification: {}".
                            format(e))

            else:
                return ResponseUtil(False, 404).message_response(
                    "Task already completed, Can't change status.")

            return ResponseUtil(True,
                                200).message_response("updated successfully")
        else:
            return ResponseUtil(False, 404).message_response("Task not found")


class StudentPackage(Document):
    _id = fields.ObjectIdField()
    studnet_id = fields.StringField()
    buddy_id = fields.StringField()
