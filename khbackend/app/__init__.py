from flask import Flask
import os
from mongoengine import connect, Document
from werkzeug.contrib.fixers import ProxyFix
from flask_cors import CORS
import pymongo

app = Flask(__name__, static_url_path="/static")
CORS(app)

""" Setting up the environment from the mainconf """

app.config.from_object('conf.mainconf.DevelopmentConfig')

""" Initiating the logger"""
logger = app.logger
logger.setLevel('DEBUG')

""" Initiating mongodb db object """

try:
    db = connect('khdb',username='khuser',password='khpass02')
    dbnames = db.database_names()

except pymongo.errors.ServerSelectionTimeoutError as err:
    print(err)

"""Set the app secret to use sessions """
app.secret_key = 'HRU5$e#/3yX!w@#tRsN!jmN]LWX/,?RT'

from app import api
