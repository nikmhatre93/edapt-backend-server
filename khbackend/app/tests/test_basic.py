from app import app
import unittest
import tempfile
import xmlrunner,json
from mongoengine import connect
from app.tests.test_helper import login, register, decode

db_client = connect(app.config.get('MONGODB_DATABASE'), username=app.config.get('MONGODB_USERNAME'), password=app.config.get('MONGODB_PASSWORD'))

class BasicTest(unittest.TestCase):

	def setUp(self):
		self.db_client = db_client
		self.app = app.test_client()

	def test_index(self):
		response = self.app.get('/')
		message = json.loads(response.data.decode('utf-8'))['message']
		assert "Hello from the root" in message

	def test_register(self):
		register_response = register("testbuddy1@tweeny.in", "12345", "buddy")
		
		if register_response['code'] == 200 or register_response['code'] == 403:
			assert True

		else:
			assert False

	def test_user_login(self):
		login_response = login("testbuddy1@tweeny.in", "12345", "buddy")

		if login_response['code'] == 200:
			assert True

		elif login_response['code'] == 403:
			response = register("testbuddy1@tweeny.in", "12345", "buddy")
			login_response = login("testbuddy1@tweeny.in", "12345", "buddy")
			if login_response['code'] == 200:
				assert True

	def tearDown(self):
		self.db_client.close()

if __name__ == '__main__':
	unittest.main()

