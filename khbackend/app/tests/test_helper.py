from app import app
import unittest
import tempfile
import xmlrunner,json
from mongoengine import connect

def decode(response):
	return json.loads(response.data.decode('utf-8'))

def register(username, password, role_type="BUDDY"):
	response = app.test_client().post('/register', data=json.dumps({
			"username":username,
			"password":password,
			"role_type":role_type,
			"first_name": "Test",
			"last_name": "User",
			"contact" : "9999999999",
			"token" : "",
			"login_type" : "knowhassels"
	  	}), follow_redirects=True)

	register_response = decode(response)
	return register_response

def login(username, password, login_as="BUDDY"):
	response = app.test_client().post('/login', data=json.dumps({
		"username":username,
		"password":password,
		"type":"knowhassels",
		"login_as":login_as
		}), follow_redirects=True)
	
	login_response = json.loads(response.data.decode('utf-8'))

	return login_response